-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 35.225.62.120    Database: ebloom
-- ------------------------------------------------------
-- Server version	5.7.14-google-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED='230ba302-d675-11e8-869f-42010a800059:1-293223,
443134b8-d924-11e8-aeee-42010a8003b1:1-41953,
45de5e6e-d987-11e8-bc5e-42010a800059:1-605245,
a9afef6b-ce41-11e8-9a28-42010a800138:1-1491065';

--
-- Table structure for table `aluno`
--

DROP TABLE IF EXISTS `aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aluno` (
  `aluno` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nome` varchar(45) DEFAULT NULL,
  `sobrenome` varchar(45) DEFAULT NULL,
  `ra` varchar(45) NOT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`aluno`),
  KEY `fk_aluno_user1_idx` (`user`),
  CONSTRAINT `fk_aluno_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6509 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assunto`
--

DROP TABLE IF EXISTS `assunto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assunto` (
  `assunto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `descricao` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`assunto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cronograma`
--

DROP TABLE IF EXISTS `cronograma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronograma` (
  `cronograma` int(11) NOT NULL AUTO_INCREMENT,
  `grade_curricular` int(11) NOT NULL,
  `curso` int(11) NOT NULL,
  `disciplina` int(11) NOT NULL,
  `dia_semana` enum('DOMINGO','SEGUNDA','TERCA','QUARTA','QUINTA','SEXTA','SABADO') DEFAULT NULL,
  `horario` time DEFAULT NULL,
  PRIMARY KEY (`cronograma`,`grade_curricular`,`curso`,`disciplina`),
  KEY `fk_cronograma_grade_curricular1_idx` (`grade_curricular`,`curso`,`disciplina`),
  CONSTRAINT `fk_cronograma_grade_curricular1` FOREIGN KEY (`grade_curricular`, `curso`, `disciplina`) REFERENCES `grade_curricular` (`grade_curricular`, `curso`, `disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `curso` int(11) NOT NULL AUTO_INCREMENT COMMENT 'coordenador do curso',
  `nome` varchar(45) DEFAULT NULL,
  `qualidade` int(11) DEFAULT NULL,
  `professor` int(11) NOT NULL,
  PRIMARY KEY (`curso`,`professor`),
  KEY `fk_curso_professor1_idx` (`professor`),
  CONSTRAINT `fk_curso_professor1` FOREIGN KEY (`professor`) REFERENCES `professor` (`professor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `detalhe`
--

DROP TABLE IF EXISTS `detalhe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalhe` (
  `detalhe` int(11) NOT NULL AUTO_INCREMENT,
  `disciplina` int(11) NOT NULL,
  `tipo` enum('T','P','S') NOT NULL,
  `numero` int(11) NOT NULL,
  `peso` float DEFAULT NULL,
  `bimestre` int(11) DEFAULT NULL,
  PRIMARY KEY (`detalhe`,`disciplina`),
  KEY `fk_detalhe_disciplina1_idx` (`disciplina`),
  CONSTRAINT `fk_detalhe_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3110 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disciplina`
--

DROP TABLE IF EXISTS `disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplina` (
  `disciplina` int(11) NOT NULL AUTO_INCREMENT,
  `professor` int(11) NOT NULL,
  `curso` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `moodle` varchar(500) DEFAULT NULL,
  `semestralidade` enum('AN','S1','S2') DEFAULT NULL,
  `qtdP` int(11) DEFAULT NULL,
  `qtdT` int(11) DEFAULT NULL,
  `kP` float DEFAULT NULL,
  `kT` float DEFAULT NULL,
  `ementa` varchar(500) DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `carga_horaria` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`disciplina`,`professor`,`curso`),
  KEY `fk_disciplina_professor1_idx` (`professor`),
  KEY `fk_disciplina_curso1_idx` (`curso`),
  CONSTRAINT `fk_disciplina_curso1` FOREIGN KEY (`curso`) REFERENCES `curso` (`curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disciplina_professor1` FOREIGN KEY (`professor`) REFERENCES `professor` (`professor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=572 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estagio`
--

DROP TABLE IF EXISTS `estagio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estagio` (
  `estagio` int(11) NOT NULL AUTO_INCREMENT,
  `aluno` int(11) NOT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `empresa` varchar(45) DEFAULT NULL,
  `inicio` date NOT NULL,
  `termino` date DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`estagio`,`aluno`),
  KEY `fk_estagio_aluno1_idx` (`aluno`),
  CONSTRAINT `fk_estagio_aluno1` FOREIGN KEY (`aluno`) REFERENCES `aluno` (`aluno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grade_curricular`
--

DROP TABLE IF EXISTS `grade_curricular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grade_curricular` (
  `grade_curricular` int(11) NOT NULL AUTO_INCREMENT,
  `curso` int(11) NOT NULL,
  `disciplina` int(11) NOT NULL,
  `serie` int(11) DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`grade_curricular`,`curso`,`disciplina`),
  KEY `fk_curso_has_disciplina_disciplina1_idx` (`disciplina`),
  KEY `fk_curso_has_disciplina_curso1_idx` (`curso`),
  CONSTRAINT `fk_curso_has_disciplina_curso1` FOREIGN KEY (`curso`) REFERENCES `curso` (`curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_curso_has_disciplina_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inscricao`
--

DROP TABLE IF EXISTS `inscricao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscricao` (
  `inscricao` int(11) NOT NULL AUTO_INCREMENT,
  `disciplina` int(11) NOT NULL,
  `matricula` int(11) NOT NULL,
  `aluno` int(11) NOT NULL,
  `curso` int(11) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `media_prova` float DEFAULT NULL,
  `media_trabalho` float DEFAULT NULL,
  `media_final` float DEFAULT NULL,
  `ranking_disciplina` int(11) DEFAULT NULL,
  `ranking_disciplina_anterior` int(11) DEFAULT NULL,
  `origem` int(11) NOT NULL,
  `situacao_inicial` int(11) NOT NULL,
  `situacao_final` int(11) DEFAULT NULL,
  `grupo` int(11) DEFAULT '1',
  `turma` int(11) DEFAULT '1',
  `laboratorio` int(11) DEFAULT '1',
  `monitor` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`inscricao`,`disciplina`,`matricula`,`aluno`,`curso`,`origem`,`situacao_inicial`),
  UNIQUE KEY `UK_inscricao_inscricao` (`inscricao`),
  KEY `fk_matriculado_disciplina1_idx` (`disciplina`),
  KEY `fk_matricula_situacao1_idx` (`origem`),
  KEY `fk_matricula_situacao2_idx` (`situacao_inicial`),
  KEY `fk_matricula_situacao3_idx` (`situacao_final`),
  KEY `fk_inscricao_matricula1_idx` (`matricula`,`aluno`,`curso`),
  CONSTRAINT `fk_inscricao_matricula1` FOREIGN KEY (`matricula`, `aluno`, `curso`) REFERENCES `matricula` (`matricula`, `aluno`, `curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_situacao1` FOREIGN KEY (`origem`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_situacao2` FOREIGN KEY (`situacao_inicial`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_situacao3` FOREIGN KEY (`situacao_final`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matriculado_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=119070 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interesse`
--

DROP TABLE IF EXISTS `interesse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interesse` (
  `interesse` int(11) NOT NULL AUTO_INCREMENT,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `assunto` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`interesse`,`assunto`,`user`),
  KEY `fk_interesse_assunto1_idx` (`assunto`),
  KEY `fk_interesse_user1_idx` (`user`),
  CONSTRAINT `fk_interesse_assunto1` FOREIGN KEY (`assunto`) REFERENCES `assunto` (`assunto`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_interesse_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `leciona`
--

DROP TABLE IF EXISTS `leciona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leciona` (
  `leciona` int(11) NOT NULL AUTO_INCREMENT,
  `disciplina` int(11) NOT NULL,
  `professor` int(11) NOT NULL,
  `notificado` tinyint(4) DEFAULT '0',
  `status` int(11) DEFAULT '0' COMMENT 'tipo de professor (coordenador, regular, integral)',
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`leciona`,`disciplina`,`professor`),
  KEY `fk_leciona_disciplina1_idx` (`disciplina`),
  KEY `fk_leciona_professor1` (`professor`),
  CONSTRAINT `fk_leciona_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_leciona_professor1` FOREIGN KEY (`professor`) REFERENCES `professor` (`professor`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=744 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matricula`
--

DROP TABLE IF EXISTS `matricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matricula` (
  `matricula` int(11) NOT NULL AUTO_INCREMENT,
  `aluno` int(11) NOT NULL,
  `curso` int(11) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `serie` int(11) NOT NULL,
  `exercicio` year(4) DEFAULT NULL,
  `periodo` enum('DIURNO','NOTURNO','MATUTINO') DEFAULT NULL,
  `coeficiente_rendimento` float DEFAULT NULL,
  `ranking_curso` int(11) DEFAULT NULL,
  `ranking_serie` int(11) DEFAULT NULL,
  `origem` int(11) NOT NULL,
  `situacao_inicial` int(11) NOT NULL,
  `situacao_final` int(11) DEFAULT NULL,
  PRIMARY KEY (`matricula`,`aluno`,`curso`,`origem`,`situacao_inicial`),
  UNIQUE KEY `UK_matricula_matricula` (`matricula`),
  KEY `fk_desempenho_aluno1_idx` (`aluno`),
  KEY `fk_desempenho_situacao1_idx` (`origem`),
  KEY `fk_desempenho_situacao2_idx` (`situacao_inicial`),
  KEY `fk_desempenho_situacao3_idx` (`situacao_final`),
  KEY `fk_matricula_curso1_idx` (`curso`),
  CONSTRAINT `fk_desempenho_aluno1` FOREIGN KEY (`aluno`) REFERENCES `aluno` (`aluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_desempenho_situacao1` FOREIGN KEY (`origem`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_desempenho_situacao2` FOREIGN KEY (`situacao_inicial`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_desempenho_situacao3` FOREIGN KEY (`situacao_final`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_curso1` FOREIGN KEY (`curso`) REFERENCES `curso` (`curso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15295 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nota`
--

DROP TABLE IF EXISTS `nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota` (
  `nota` int(11) NOT NULL AUTO_INCREMENT,
  `inscricao` int(11) NOT NULL,
  `detalhe` int(11) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lancado` tinyint(4) NOT NULL,
  `valor` float DEFAULT NULL,
  PRIMARY KEY (`nota`,`inscricao`,`detalhe`),
  KEY `fk_nota_matricula1_idx` (`inscricao`),
  KEY `fk_nota_detalhe1_idx` (`detalhe`),
  CONSTRAINT `fk_nota_detalhe1` FOREIGN KEY (`detalhe`) REFERENCES `detalhe` (`detalhe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_nota_matricula1` FOREIGN KEY (`inscricao`) REFERENCES `inscricao` (`inscricao`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=898815 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notificacao`
--

DROP TABLE IF EXISTS `notificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificacao` (
  `notificacao` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `titulo` varchar(75) DEFAULT NULL,
  `mensagem` varchar(500) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notificacao`,`user`),
  KEY `fk_notificacao_mensagem1_idx` (`mensagem`),
  KEY `fk_notificacao_user1` (`user`),
  CONSTRAINT `fk_notificacao_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `previsao_disciplina`
--

DROP TABLE IF EXISTS `previsao_disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `previsao_disciplina` (
  `previsao` int(11) NOT NULL AUTO_INCREMENT,
  `inscricao` int(11) NOT NULL,
  `aprovacao` float DEFAULT NULL,
  `reprovacao` float DEFAULT NULL,
  `resultado` int(11) DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`previsao`,`inscricao`),
  KEY `fk_inscricao_matricula2_idx` (`inscricao`),
  CONSTRAINT `fk_inscricao_matricula2_idx` FOREIGN KEY (`inscricao`) REFERENCES `inscricao` (`inscricao`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5613 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `previsao_evasao`
--

DROP TABLE IF EXISTS `previsao_evasao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `previsao_evasao` (
  `evasao` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` int(11) NOT NULL,
  `prob_permanencia` float DEFAULT NULL,
  `prob_evasao` float DEFAULT NULL,
  `resultado_evasao` int(11) DEFAULT NULL,
  `prob_reprovacao` float DEFAULT NULL,
  `prob_promocao` float DEFAULT NULL,
  `resultado_reprovacao` int(11) DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`evasao`,`matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=1843 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prof_users`
--

DROP TABLE IF EXISTS `prof_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prof_users` (
  `email` text,
  `pwd` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `professor`
--

DROP TABLE IF EXISTS `professor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor` (
  `professor` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `nome` varchar(45) NOT NULL,
  `sobrenome` varchar(45) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lattes` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`professor`),
  KEY `fk_professor_user1_idx` (`user`),
  CONSTRAINT `fk_professor_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `score_ml`
--

DROP TABLE IF EXISTS `score_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score_ml` (
  `score_ml` int(11) NOT NULL AUTO_INCREMENT,
  `valor_score` float DEFAULT NULL,
  `prova` varchar(255) DEFAULT NULL,
  `trabalho` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`score_ml`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `situacao`
--

DROP TABLE IF EXISTS `situacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `situacao` (
  `situacao` int(11) NOT NULL AUTO_INCREMENT,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campo` enum('MATRICULA','DISCIPLINA') DEFAULT NULL,
  `tipo` enum('ORIGEM','SITUACAO') DEFAULT NULL,
  `cod` varchar(5) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `observacao` varchar(1000) DEFAULT NULL,
  `aprovacao` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`situacao`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `pwd` varchar(45) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo` enum('aluno','professor','admin') NOT NULL,
  `foto` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'ebloom'
--
/*!50003 DROP FUNCTION IF EXISTS `aprovacao_disciplina` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `aprovacao_disciplina`(cod varchar(10), ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE total_alunos float DEFAULT 0;
	DECLARE total_alunos_aprovados float DEFAULT 0;
	DECLARE taxa_aprovacao float DEFAULT 0;
	SET total_alunos = (
		SELECT count(*) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat ON ins.matricula = mat.matricula
		INNER JOIN ebloom.disciplina as dis ON dis.disciplina = ins.disciplina 
		INNER JOIN ebloom.previsao_disciplina as prev ON prev.inscricao = ins.inscricao
		WHERE mat.exercicio = ano AND dis.codigo = cod
	);

	SET total_alunos_aprovados = (
		SELECT count(*) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat ON mat.matricula = ins.matricula
		INNER JOIN ebloom.disciplina as dis ON dis.disciplina = ins.disciplina
		INNER JOIN ebloom.previsao_disciplina as prev ON prev.inscricao = ins.inscricao
		WHERE ebloom.media_final(ins.inscricao) >= 6 AND dis.codigo = cod AND mat.exercicio = ano 
	);
	
	SET taxa_aprovacao = total_alunos_aprovados/NULLIF(total_alunos,0);

	RETURN taxa_aprovacao;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `coeficiente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `coeficiente`(m int) RETURNS float
BEGIN
	DECLARE coeficiente float DEFAULT 0;
	CALL COEF(m, coeficiente);
	RETURN coeficiente;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `media_ano_bimestre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `media_ano_bimestre`(d int, bimestre int, ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE media float DEFAULT 0;  
   	CALL MEDIA_HISTORICA(d,bimestre,ano, media);
	RETURN ROUND(media,1);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `media_final` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `media_final`(i int) RETURNS float
    DETERMINISTIC
BEGIN
		DECLARE media float DEFAULT 0;     
    CALL MEDIA_DISCIPLINA_2(i, media);          
		RETURN media;
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `media_prova` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `media_prova`(i int) RETURNS float
    DETERMINISTIC
BEGIN
		DECLARE media float DEFAULT 0;
      
      IF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
         ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i)  = 'AN'
      
      THEN           
    	    CALL MPAN(i, media);

      ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i) = 'S1'
      THEN          
          CALL MPS1(i, media);

      ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i) = 'S2'

      THEN
          CALL MPS1(i, media);

      END IF;
          
		RETURN ROUND(media,1);
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `media_prova_parcial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `media_prova_parcial`(insc int) RETURNS float
    DETERMINISTIC
BEGIN
     SET @outvar = 0.0; 
    CALL ebloom.MEDIA_PROVA_PARCIAL(insc, @outvar);
    RETURN ROUND(@outvar,1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `media_trabalho` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `media_trabalho`(i int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE media float DEFAULT 0;  
   	CALL MT(i, media);
	RETURN ROUND(media,1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `media_trabalho_parcial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `media_trabalho_parcial`(insc int) RETURNS float
    DETERMINISTIC
BEGIN
      
      SET @outvar = 0.0;
      CALL ebloom.MEDIA_TRAB_PARCIAL(insc, @outvar);
      RETURN ROUND(@outvar,1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `previsao_aprovacao` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `previsao_aprovacao`(exer int) RETURNS float
    DETERMINISTIC
BEGIN
        DECLARE evasao float DEFAULT 0;  
        CALL  PREVISAO_APROVACAO(exer, evasao);
        RETURN ROUND(NULLIF(evasao,0),2);
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `previsao_aprovacao_disciplina` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `previsao_aprovacao_disciplina`(cod VARCHAR(10), ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE total_alunos float DEFAULT 0;
	DECLARE total_alunos_aprovados float DEFAULT 0;
	DECLARE taxa_aprovacao float DEFAULT 0;
	SET total_alunos = (
		SELECT COUNT(*) FROM inscricao i INNER JOIN previsao_disciplina pd ON i.inscricao = pd.inscricao
		INNER JOIN disciplina d ON i.disciplina = d.disciplina INNER JOIN matricula m ON i.matricula = m.matricula 
		WHERE m.exercicio = ano AND d.codigo = cod
	);

	SET total_alunos_aprovados = (
		SELECT count(*) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.previsao_disciplina as prev ON prev.inscricao = ins.inscricao
		INNER JOIN ebloom.matricula as mat ON mat.matricula = ins.matricula
		INNER JOIN ebloom.disciplina as dis ON dis.disciplina = ins.disciplina
		WHERE prev.resultado = 0 AND dis.codigo = cod AND mat.exercicio = ano 
	);
	
	SET taxa_aprovacao = total_alunos_aprovados/NULLIF(total_alunos,0);

	RETURN taxa_aprovacao;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `previsao_evasao` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `previsao_evasao`(exer int) RETURNS float
    DETERMINISTIC
BEGIN
        DECLARE evasao float DEFAULT 0;  
        CALL  PREVISAO_EVASAO(exer, evasao);
        RETURN ROUND(NULLIF(evasao,0),2);
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `taxa_evasao_1serie_ano` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `taxa_evasao_1serie_ano`(ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE matriculas_ano float DEFAULT 0;
	DECLARE desistentes float DEFAULT 0;
	DECLARE taxa_evasao float DEFAULT 0;
	
	SET matriculas_ano = (SELECT COUNT(*) from ebloom.matricula WHERE serie = 1 and exercicio=ano);

	SET desistentes = (SELECT COUNT(*) from ebloom.matricula as mat 
	INNER JOIN ebloom.situacao as sit ON mat.situacao_final = sit.situacao 
	WHERE serie = 1 and exercicio=ano and sit.nome in ("JUB","TRG","INT","CAN","CANC","DLG""DES","FAL","TFEE","NRM"));
	
	IF ((matriculas_ano) != 0)
	THEN
		SET taxa_evasao = 1 - ((matriculas_ano - desistentes)/(matriculas_ano));
		IF ((taxa_evasao) >= 0)
		THEN
			RETURN taxa_evasao;
		ELSE
			RETURN NULL;
		END IF;
	ELSE
		RETURN NULL;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `taxa_evasao_ano` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `taxa_evasao_ano`(ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE matriculas_ano float DEFAULT 0;
	DECLARE matriculas_ano_anterior float DEFAULT 0;
	DECLARE egressos float DEFAULT 0;
	DECLARE ingressantes float DEFAULT 0;
	DECLARE taxa_evasao float DEFAULT 0;
	DECLARE formados float DEFAULT 0;
	DECLARE saida float DEFAULT 0;
	DECLARE matr_ausentes_diurno float DEFAULT 0;
	DECLARE matr_ausentes_noturno float DEFAULT 0;
	SET matriculas_ano = ebloom.total_alunos_ano(ano);
	SET matriculas_ano_anterior = ebloom.total_alunos_ano(ano-1);
	SET egressos = ebloom.total_alunos_saida_ano(ano-1);
	SET ingressantes = ebloom.total_alunos_novos_ano(ano);
	SET formados = ebloom.total_alunos_formados_ano(ano-1);
	SET matr_ausentes_diurno = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "DIURNO" AND mat.exercicio = ano-1
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano)) = 0
	);
	SET matr_ausentes_noturno = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "NOTURNO" AND mat.exercicio = (ano-1)
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano)) = 0
	);
	SET matriculas_ano_anterior = matriculas_ano_anterior + matr_ausentes_diurno + matr_ausentes_noturno;
	SET saida = egressos + formados;
	IF ((matriculas_ano_anterior - saida) != 0)
	THEN
		SET taxa_evasao = 1 - ((matriculas_ano - ingressantes)/(matriculas_ano_anterior - saida));
		IF ((taxa_evasao) >= 0)
		THEN
			RETURN taxa_evasao;
		ELSE
			RETURN NULL;
		END IF;
	ELSE
		RETURN NULL;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_alunos_ano` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `total_alunos_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	SET total = (
		SELECT count(*) FROM ebloom.matricula WHERE exercicio = ano
	);
	RETURN total;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_alunos_aprovados_disciplina_atual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `total_alunos_aprovados_disciplina_atual`(idDis int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT COUNT(ins.inscricao) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis and ebloom.media_final(ins.inscricao) >= 6
		);
	RETURN media;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_alunos_disciplina_atual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `total_alunos_disciplina_atual`(idDis int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT COUNT(ins.inscricao) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis
		);
	RETURN media;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_alunos_formados_ano` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `total_alunos_formados_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	DECLARE total_formados int DEFAULT 0;
	DECLARE total_fmdausentes int DEFAULT 0;
	DECLARE total_fmdausentesnoturno int DEFAULT 0;
	SET total_formados = (
		SELECT count(*) 
		FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and mat.situacao_final = "FMD"
	);
	SET total_fmdausentes = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "DIURNO" AND mat.exercicio = ano
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano+1)) = 0
	);
	SET total_fmdausentesnoturno = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "NOTURNO" AND mat.exercicio = (ano)
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano+1)) = 0
	);

	SET total = total_formados + total_fmdausentes + total_fmdausentesnoturno;
	RETURN total;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_alunos_novos_ano` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `total_alunos_novos_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	SET total = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit on sit.situacao = mat.origem and sit.nome IN ("VES","QLF","TRFE","NHAB")
		WHERE mat.exercicio = ano
	);
	RETURN total;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_alunos_reprovados_disciplina_atual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `total_alunos_reprovados_disciplina_atual`(idDis int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT COUNT(ins.inscricao) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis and ebloom.media_final(ins.inscricao) < 6
		);
	RETURN media;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_alunos_saida_ano` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `total_alunos_saida_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	SET total = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit on sit.situacao = mat.situacao_final and sit.nome IN ("JUB","TRG","INT","CAN","CANC","DLG""DES","FAL","TFEE","NRM")
		WHERE mat.exercicio = ano
	);
	RETURN total;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `val_media_disciplina_atual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `val_media_disciplina_atual`(idDis int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT AVG(ebloom.media_final(ins.inscricao)) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio = ano
		WHERE ins.disciplina = idDis
		);
	RETURN media;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `val_media_disciplina_historico` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `val_media_disciplina_historico`(idDis int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT AVG(ebloom.media_final(ins.inscricao)) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis
		);
	RETURN media;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `verificar_nota_lancada` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `verificar_nota_lancada`(i int, d int) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	DECLARE ano,qtd int DEFAULT 0;
	SET ano = (SELECT mat.exercicio FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.inscricao = i);
	SET qtd = (
		SELECT count(*) 
		FROM ebloom.nota as nota
		INNER JOIN ebloom.inscricao as ins ON ins.inscricao = nota.inscricao
		INNER JOIN ebloom.matricula as mat ON mat.matricula = ins.matricula and mat.exercicio = ano
		WHERE nota.detalhe = d
	);
	
	IF (qtd > 0) THEN
	RETURN TRUE;
	ELSE 
	RETURN FALSE;
	END IF;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `COEF` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `COEF`(IN m INT, OUT media FLOAT)
BEGIN		
		DECLARE qnt, disc, cargaHor, insc INT;
        DECLARE aux1, aux2 FLOAT;
        DECLARE n INT DEFAULT 0;
		DECLARE i INT DEFAULT 0;
        SET n = (SELECT count(inscricao) FROM ebloom.inscricao WHERE matricula = m);
		  
        SET media = 0;
        SET aux1 = 0;
        SET aux2 = 0;
        
        WHILE i<n DO
			SET insc = (SELECT inscricao FROM ebloom.inscricao WHERE matricula = m ORDER BY inscricao ASC LIMIT i,1);
                        
            IF (SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.matricula = m 
            ORDER BY ebloom.disciplina.disciplina LIMIT 1) = "S1"
            
            THEN
				SET @outvar = 0.0;
				CALL MFS1(insc,@outvar);
                SET cargaHor = (SELECT carga_horaria FROM ebloom.disciplina ORDER BY ebloom.disciplina.disciplina LIMIT 1);
                SET aux1 = @outvar*cargaHor;
                SET media = media + aux1;
                SET aux2 = aux2 + cargaHor;
                
				SET i = i + 1;
				SET aux1 = 0;
			
            ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.matricula = m 
            ORDER BY ebloom.disciplina.disciplina LIMIT 1) = "AN" 
			
            THEN
				SET @outvar = 0.0;
				CALL MFAN(insc,@outvar);  
                SET cargaHor = (SELECT carga_horaria FROM ebloom.disciplina ORDER BY ebloom.disciplina.disciplina LIMIT 1);
                SET aux1 = @outvar*cargaHor;
                SET media = media + aux1;
                SET aux2 = aux2 + cargaHor;
			
				SET i = i + 1;
				SET aux1 = 0;
                
			ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.matricula = m 
            ORDER BY ebloom.disciplina.disciplina LIMIT 1) = "S2" 
			
            THEN
				SET @outvar = 0.0;
				CALL MFS2(insc,@outvar);  
                SET cargaHor = (SELECT carga_horaria FROM ebloom.disciplina ORDER BY ebloom.disciplina.disciplina LIMIT 1);
                SET aux1 = @outvar*cargaHor;
                SET media = media + aux1;
                SET aux2 = aux2 + cargaHor;
			
				SET i = i + 1;
				SET aux1 = 0;
			
			END IF;
   
		END WHILE;		
        	
        SET media = media/aux2;   
        
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MEDIA_ATUAL_BIMESTRE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MEDIA_ATUAL_BIMESTRE`(IN d int)
BEGIN
       DECLARE tipo_dis VARCHAR(20) DEFAULT 0;
       DECLARE ano int DEFAULT 0;
       SET tipo_dis = (SELECT semestralidade FROM disciplina WHERE disciplina = d);
       
       SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = d
		);
       
       IF(tipo_dis = "AN")
       THEN
			SELECT 
			ROUND(AVG(ebloom.media_ano_bimestre(d, 1,mat.exercicio)),1) as b1,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 2,mat.exercicio)),1) as b2,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 3,mat.exercicio)),1) as b3,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 4,mat.exercicio)),1) as b4
			FROM ebloom.matricula as mat
			INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
			INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
			WHERE mat.exercicio = ano;
       ELSE
       	   	SELECT 
		   	ROUND(ebloom.media_ano_bimestre(d, 1,mat.exercicio),1) as b1,
		   	ROUND(ebloom.media_ano_bimestre(d, 2,mat.exercicio),1) as b2
		   	FROM ebloom.matricula as mat
		   	INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
		   	INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
	       	WHERE mat.exercicio = ano;
	    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MEDIA_DISCIPLINA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MEDIA_DISCIPLINA`(IN insc int, OUT media_final float)
BEGIN

        DECLARE semest varchar(2);
        DECLARE p1,s1,p2,s2,p3,p4,soma_prova,soma_trabalho,peso_prova,peso_trabalho,media_prova,media_trabalho,
                media_prova_parcial, media_trabalho_parcial float DEFAULT 0;
        DECLARE provas_q_sairam, trabalhos_q_sairam, qnt_provas, qnt_trabalhos int DEFAULT 0;
        
        SET semest = (SELECT d.semestralidade FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                      WHERE i.inscricao = insc);

        SET qnt_provas = (SELECT qtdP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET provas_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                               WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

        SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                  WHERE n.inscricao = insc AND d.tipo = 'T');

        SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);

        SET peso_prova = (SELECT kP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET peso_trabalho = (SELECT kT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);
        
        #SELECT semest, qnt_provas, provas_q_sairam, qnt_trabalhos, trabalhos_q_sairam;

        IF(qnt_provas = 0)
        THEN       
             #SELECT 'trabalho';    
             CALL MT(insc, @mt);
             SET media_final = ROUND(@mt,1);
        ELSE
  
          IF (provas_q_sairam < qnt_provas)
          THEN
                  
                  SET soma_prova = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                                    INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'P');
  
                  SET soma_trabalho = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                                       INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'T');
  
                  SET provas_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                         WHERE n.inscricao = insc AND (d.tipo = 'P'));
  
                  SET media_prova_parcial = soma_prova / provas_q_sairam;
                  SET media_trabalho_parcial = soma_trabalho / trabalhos_q_sairam;
                  
                  #SELECT "parcial", media_prova_parcial, media_trabalho_parcial, peso_prova, peso_trabalho;
  
                  SET media_final = (IFNULL(media_prova_parcial,0) * peso_prova + IFNULL(media_trabalho_parcial,0) * peso_trabalho) 
                                      / NULLIF((peso_prova + peso_trabalho),0);
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 6
          THEN           
                  # Quando todas as notas foram lançadas
                  #SELECT semest;
    
                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p2;
                  END IF;
      
                  SET s2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT s2;
                  
                  SET p3 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 3 AND i.inscricao = insc);
      
                  #SELECT p3;
      
                  SET p4 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 4 AND i.inscricao = insc);
      
                  #SELECT p4;
      
                  IF(s2 > p3)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p3;
                  END IF;
      
                  IF(s2 > p4)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p4;
                  END IF;
                  
                  #SELECT soma_prova;
                  SET media_prova = soma_prova / 10;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
              #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
  
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 3
          THEN 
                  #SELECT "AN";

                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p2;
                  END IF;
  
                  SET media_prova = soma_prova / 2;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
                  SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
                  #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
                
  
          ELSEIF (semest) = "S1" OR (semest) = "S2"
          THEN
              #SELECT "S1 ou S1";
              
              SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT s1;
              
              SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT p1;
  
              SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 2 AND i.inscricao = insc);
  
              #SELECT p2;
  
              IF(s1 > p1)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p1;
              END IF;
  
              IF(s1 > p2)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p2;
              END IF;
  
              #SELECT soma_prova;
  
              SET media_prova = soma_prova / 2;
              CALL MT(insc, @mt);
              SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0);
              
              #SELECT "total - S1 ou S2", media_trabalho, media_prova, peso_prova, peso_trabalho;
                 
          END IF;  

      END IF;
     
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MEDIA_DISCIPLINA_2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MEDIA_DISCIPLINA_2`(IN insc int, OUT media_final float)
BEGIN

        DECLARE semest varchar(2);
        DECLARE p1,s1,p2,s2,p3,p4,soma_prova,soma_trabalho,peso_prova,peso_trabalho,media_prova,media_trabalho,
                media_prova_parcial, media_trabalho_parcial float DEFAULT 0;
        DECLARE provas_q_sairam, trabalhos_q_sairam, qnt_provas, qnt_trabalhos int DEFAULT 0;
        
        SET semest = (SELECT d.semestralidade FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                      WHERE i.inscricao = insc);

        SET qnt_provas = (SELECT qtdP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET provas_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                               WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

        SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                  WHERE n.inscricao = insc AND d.tipo = 'T');

        SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);

        SET peso_prova = (SELECT kP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET peso_trabalho = (SELECT kT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);
        
        #SELECT semest, qnt_provas, provas_q_sairam, qnt_trabalhos, trabalhos_q_sairam;

        IF(qnt_provas = 0)
        THEN       
             #SELECT 'MATERIAS FULL TRABALHO';    
             CALL MT(insc, @mt);
             SET media_final = ROUND(@mt,1);
        ELSE
  
            IF (provas_q_sairam < qnt_provas)
            THEN
                  
                IF (semest) = "AN" AND (qnt_provas) = 6
                THEN           
                  #PARCIAL DE MATERIA ANUAL COM 6 PROVAS

                    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                              WHERE n.inscricao = insc AND d.tipo = 'T');

                    #SELECT trabalhos_q_sairam;

                    SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                                           ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

                    #SELECT provas_q_sairam;
                     
                    SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                                d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT s1;
                  
                    SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT p1;
      
                    SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 2 AND i.inscricao = insc);
      
                    #SELECT p1, p2, s1;
                IF (NOT ISNULL(p1) OR NOT ISNULL (s1))
                THEN
      
                    IF(COALESCE(s1,0) > COALESCE(p1,0))              
                    THEN     
                       # SELECT "entrou";         
                        SET soma_prova = soma_prova +  s1;
                       # SELECT soma_prova;
                    ELSE
                        #SELECT "entrou errado";
                        SET soma_prova = soma_prova +  p1;
                    END IF;
                END IF;


                IF (NOT ISNULL(p2) OR NOT ISNULL (s1))
                THEN
                    IF(COALESCE(s1,0) > COALESCE(p2,0))              
                    THEN              
                        SET soma_prova = soma_prova +  s1;
                    ELSE
                        SET soma_prova = soma_prova +  p2;
                    END IF;
                END IF;
      
                    SET s2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                                d.numero = 2 AND i.inscricao = insc);
      
                    #SELECT s2;
                  
                    SET p3 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 3 AND i.inscricao = insc);
      
                    #SELECT p3;
      
                    SET p4 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 4 AND i.inscricao = insc);
      
                    #SELECT p4;
                IF (NOT ISNULL(p3) OR NOT ISNULL (s2))
                THEN
                    IF(COALESCE(s2,0) > COALESCE(p3,0))              
                    THEN    
                                   
                        SET soma_prova = soma_prova +  s2;
                    ELSE
                       # SELECT "entrou certo na p3";
                        SET soma_prova = soma_prova +  p3;
                       # SELECT soma_prova;
                    END IF;
                END IF;
                    
              IF (NOT ISNULL(p4) OR NOT ISNULL (s2))
                THEN

                    IF(COALESCE(s2,0) > COALESCE(p4,0))              
                    THEN               
                        SET soma_prova = soma_prova +  s2;
                    ELSE
                        SET soma_prova = soma_prova + p4;
                    END IF;

             END IF;
                
                    #SELECT soma_prova;   
                    
                    IF(NOT ISNULL(s1))
                    THEN
                        SET provas_q_sairam = provas_q_sairam - 1;
                    END IF;

                    IF(NOT ISNULL(s1 AND s2))
                    THEN
                        SET provas_q_sairam = provas_q_sairam - 2;
                    END IF;

                    #SELECT provas_q_sairam; 
                    #SELECT soma_prova; 
                                   
                    SET media_final = NULLIF(soma_prova,0) / NULLIF(provas_q_sairam,0);
                    CALL MEDIA_TRAB_PARCIAL(insc, @mt);
                    SET media_trabalho = ROUND(@mt,1);

                    #SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                        #NULLIF((peso_prova + peso_trabalho),0); 
  
                #SELECT "total - AN - PARCIAL",media_prova, media_trabalho, peso_prova, peso_trabalho;
  
  
                ELSEIF (semest) = "AN" AND ((qnt_provas) = 3 OR (qnt_provas) = 2)
                THEN                    
                    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                              WHERE n.inscricao = insc AND d.tipo = 'T');

                    SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                                           ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S')); 
                    #SELECT "AN";

                    SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                             INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                             d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT s1;
                  
                    SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT p1;
      
                    SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                    #SELECT p2;


                IF (NOT ISNULL(s1) OR NOT ISNULL (p1))
                  THEN
                    IF(IFNULL(s1,0) > IFNULL(p1,0))              
                    THEN 
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                        SET soma_prova = soma_prova + p1;
                    END IF;
                 END IF;

                IF (NOT ISNULL(s1) OR NOT ISNULL (p2))
                  THEN
                    IF(IFNULL(s1,0) > IFNULL(p2,0))              
                    THEN   
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                      SET soma_prova = soma_prova + p2;
                    END IF;
                 END IF;

                    #SELECT soma_prova, provas_q_sairam;
                    SET media_prova = NULLIF(soma_prova,0) / NULLIF(provas_q_sairam,0);
                    CALL MEDIA_TRAB_PARCIAL(insc, @mt);
                    SET media_trabalho = ROUND(@mt,1);

                    
  
                    SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                       NULLIF((peso_prova + peso_trabalho),0); 
    
                    #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
                
  
                ELSEIF (semest) = "S1" OR (semest) = "S2"
                THEN
                    #SELECT "S1 ou S1";

                    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                              WHERE n.inscricao = insc AND d.tipo = 'T');

                    SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                                           ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S')); 
              
                    SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
  
                    #SELECT s1;
                
                    SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
    
                    #SELECT p1;
    
                    SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
    
                    #SELECT p2;
  

                    IF (NOT ISNULL(s1) OR NOT ISNULL (p1))
                      THEN
                    IF(IFNULL(s1,0) > IFNULL(p1,0))              
                    THEN   
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                        SET soma_prova = soma_prova + p1;
                    END IF;
                  END IF;


                  IF (NOT ISNULL(s1) OR NOT ISNULL (p2))
                    THEN
                    IF(IFNULL(s1,0) > IFNULL(p2,0))              
                    THEN    
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                        SET soma_prova = soma_prova + p2;
                    END IF;
                  END IF;
  
                    #SELECT soma_prova, provas_q_sairam;
  
                    SET media_final = NULLIF(soma_prova,0) / NULLIF(provas_q_sairam,0);
                    CALL MEDIA_TRAB_PARCIAL(insc, @mt);
                    SET media_trabalho = ROUND(@mt,1);
        
                    #SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                        #NULLIF((peso_prova + peso_trabalho),0);
                    
                    #SELECT "total - S1 ou S2", media_trabalho, media_prova, peso_prova, peso_trabalho;
                 
            END IF;  

          #JA SAIU TODAS AS NOTAS
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 6
          THEN           
                  # Quando todas as notas foram lançadas
                  #SELECT semest;
    
                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p2;
                  END IF;
      
                  SET s2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT s2;
                  
                  SET p3 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 3 AND i.inscricao = insc);
      
                  #SELECT p3;
      
                  SET p4 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 4 AND i.inscricao = insc);
      
                  #SELECT p4;
      
                  IF(s2 > p3)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p3;
                  END IF;
      
                  IF(s2 > p4)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p4;
                  END IF;
                  
                  #SELECT soma_prova;
                  SET media_prova = soma_prova / 10;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
              #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
  
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 3
          THEN 
                  #SELECT "AN";

                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p2;
                  END IF;
  
                  SET media_prova = soma_prova / 2;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
                  SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
                  #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
                
  
          ELSEIF (semest) = "S1" OR (semest) = "S2"
          THEN
              #SELECT "S1 ou S1";
              
              SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT s1;
              
              SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT p1;
  
              SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 2 AND i.inscricao = insc);
  
              #SELECT p2;
  
              IF(s1 > p1)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p1;
              END IF;
  
              IF(s1 > p2)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p2;
              END IF;
  
              #SELECT soma_prova;
  
              SET media_prova = soma_prova / 2;
              CALL MT(insc, @mt);
              SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0);
              
              #SELECT "total - S1 ou S2", media_trabalho, media_prova, peso_prova, peso_trabalho;
                 
          END IF;  

      END IF;
     
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MEDIA_HISTORICA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MEDIA_HISTORICA`(IN d int, IN b int, IN a year, OUT media_hist float)
BEGIN
        DECLARE numero_alunos int DEFAULT 0;
        DECLARE soma_provas, soma_trabalhos, peso_trabalho, peso_prova, media_hist_prova, media_hist_trabalho float DEFAULT 0;

        SET numero_alunos = (SELECT COUNT(inscricao) FROM inscricao INNER JOIN matricula m ON inscricao.matricula = m.matricula AND 
                             inscricao.aluno = m.aluno AND inscricao.curso = m.curso WHERE disciplina = d AND m.exercicio = a);

        #SELECT numero_alunos;
                            
        SET peso_trabalho = (SELECT kT FROM disciplina WHERE disciplina = d);

        #SELECT peso_trabalho;
       
        SET peso_prova = (SELECT kP FROM disciplina WHERE disciplina = d);

        #SELECT peso_prova;
      
        SET soma_provas = (SELECT SUM(nota.valor) FROM nota INNER JOIN inscricao ON nota.inscricao = inscricao.inscricao                        
                           INNER JOIN detalhe ON nota.detalhe = detalhe.detalhe INNER JOIN matricula ON
                           inscricao.matricula = matricula.matricula WHERE detalhe.disciplina = d AND
                           detalhe.tipo = "P" AND detalhe.bimestre = b AND matricula.exercicio = a); 

        #SELECT soma_provas;
                          
        SET soma_trabalhos = (SELECT SUM(nota.valor)/count(DISTINCT det.numero) FROM nota 
							INNER JOIN inscricao as ins ON nota.inscricao = ins.inscricao                        
							INNER JOIN detalhe as det ON nota.detalhe = det.detalhe 
							INNER JOIN matricula as mat ON ins.matricula = mat.matricula 
							WHERE det.disciplina = d AND det.tipo = "T" AND det.bimestre = b AND mat.exercicio = a); 

            
        #SELECT soma_trabalhos;

      
        SET media_hist_prova = soma_provas/numero_alunos;

        #SELECT media_hist_prova;
       
        SET media_hist_trabalho = soma_trabalhos/numero_alunos;

        #SELECT media_hist_trabalho;
              
       	SET media_hist = ((media_hist_prova * peso_prova) + (media_hist_trabalho * peso_trabalho)) / (peso_prova + peso_trabalho); 

        #SELECT media_hist;
         
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MEDIA_HISTORICA_BIMESTRE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MEDIA_HISTORICA_BIMESTRE`(IN d int)
BEGIN
       DECLARE tipo_dis VARCHAR(20) DEFAULT 0;
       DECLARE ano int DEFAULT 0;
       SET tipo_dis = (SELECT semestralidade FROM disciplina WHERE disciplina = d);
       
       SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = d
		);
       
       IF(tipo_dis = "AN")
       THEN
			SELECT 
			ROUND(AVG(ebloom.media_ano_bimestre(d, 1,mat.exercicio)),1) as b1,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 2,mat.exercicio)),1) as b2,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 3,mat.exercicio)),1) as b3,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 4,mat.exercicio)),1) as b4
			FROM ebloom.matricula as mat
			INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
			INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
			WHERE mat.exercicio = ano-1;
       ELSE
       	   	SELECT 
		   	ROUND(ebloom.media_ano_bimestre(d, 1,mat.exercicio),1) as b1,
		   	ROUND(ebloom.media_ano_bimestre(d, 2,mat.exercicio),1) as b2
		   	FROM ebloom.matricula as mat
		   	INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
		   	INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
	       	WHERE mat.exercicio = ano-1;
	    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MEDIA_PROVA_PARCIAL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MEDIA_PROVA_PARCIAL`(IN insc int, OUT media_prova_parcial float)
BEGIN

  DECLARE qnt_provas, provas_q_sairam int DEFAULT 0;
  DECLARE mpp, soma_prova, peso_prova float DEFAULT 0;

  SET qnt_provas = (SELECT qtdP FROM disciplina d INNER JOIN inscricao i
                    ON d.disciplina = i.disciplina WHERE i.inscricao = insc);

  SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                         ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

  SET soma_prova = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i
        ON nota.inscricao = i.inscricao INNER JOIN detalhe d
        ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'P');

  SET peso_prova = (SELECT kP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                    WHERE i.inscricao = insc);

  SET media_prova_parcial = soma_prova / provas_q_sairam;

  #SET media_prova_parcial = (mpp * peso_prova) / NULLIF(peso_prova, 0);

#SELECT soma_provas, provas_q_sairam, peso_prova;
#SELECT soma_trabalhos, provas_q_sairam, peso_trabalho;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MEDIA_TRAB_PARCIAL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MEDIA_TRAB_PARCIAL`(IN insc int, OUT media_trab_parcial float)
BEGIN

  DECLARE qnt_trabalhos,
          trabalhos_q_sairam int DEFAULT 0;
  DECLARE mtp,
          soma_trabalho, peso_trabalho float DEFAULT 0;

  SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                  WHERE n.inscricao = insc AND d.tipo = 'T');

  SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);
  SET peso_trabalho = (SELECT kT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                       WHERE i.inscricao = insc);

  SET soma_trabalho = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                       INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'T');


  SET media_trab_parcial = soma_trabalho / trabalhos_q_sairam;

  #SET media_trab_parcial = IFNULL((mtp * peso_trabalho),0) / NULLIF(peso_trabalho, 0);

#SELECT soma_provas, provas_q_sairam, peso_prova;
#SELECT soma_trabalhos, provas_q_sairam, peso_trabalho;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MFAN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MFAN`(IN i INT, OUT mf FLOAT)
BEGIN	
	
		DECLARE pesoProva, pesoTrab FLOAT;
        
        SET @outvar1 = 0.0;
        CALL MPAN(i,@outvar1);
        SET @outvar2 = 0.0;
        CALL MT(i,@outvar2);
		
        SET pesoProva = (SELECT kP 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
                        
        SET pesoTrab = (SELECT kT 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
        
        SET mf = (IFNULL(pesoProva,0) * IFNULL(@outvar1,0) + IFNULL(pesoTrab,0) * IFNULL(@outvar2,0))/(IFNULL(pesoProva,0) + IFNULL(pesoTrab,0));
        
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MFS1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MFS1`(IN i INT, OUT mf FLOAT)
BEGIN	
	
		DECLARE pesoProva, pesoTrab FLOAT;
        
        SET @outvar1 = 0.0;
        CALL MPS1(i,@outvar1);
        SET @outvar2 = 0.0;
        CALL MT(i,@outvar2);
		
        SET pesoProva = (SELECT kP 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
                        
        SET pesoTrab = (SELECT kT 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
	
        SET mf = (IFNULL(pesoProva,0) * IFNULL(@outvar1,0) + IFNULL(pesoTrab,0) * IFNULL(@outvar2,0))/(IFNULL(pesoProva,0) + IFNULL(pesoTrab,0));
        
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MFS2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MFS2`(IN i INT, OUT mf FLOAT)
BEGIN	
	
		DECLARE pesoProva, pesoTrab FLOAT;
        
        SET @outvar1 = 0.0;
        CALL MPS1(i,@outvar1);
        SET @outvar2 = 0.0;
        CALL MT(i,@outvar2);        
		
        SET pesoProva = (SELECT kP 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
                        
        SET pesoTrab = (SELECT kT 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
        
        SET mf = (IFNULL(pesoProva,0) * IFNULL(@outvar1,0) + IFNULL(pesoTrab,0) * IFNULL(@outvar2,0))/(IFNULL(pesoProva,0) + IFNULL(pesoTrab,0));
        
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MPAN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MPAN`(IN i INT, OUT media FLOAT)
BEGIN
		IF(SELECT qtdP FROM disciplina INNER JOIN inscricao ON disciplina.disciplina = inscricao.disciplina 
       WHERE inscricao = i) = 6
        THEN
			
			SET @outvar1 = 0.0;
			CALL MPS1(i,@outvar1);
			SET @outvar2 = 0.0;
			CALL MPS2(i,@outvar2);
			SET media = (2 * @outvar1 + 3 * @outvar2)/5;
            
		ELSEIF(SELECT qtdP FROM disciplina INNER JOIN inscricao ON disciplina.disciplina = inscricao.disciplina 
            WHERE inscricao = i) = 3
        THEN
			SET @outvar1 = 0.0;
            CALL MPS1(i,@outvar1);
            SET media = @outvar1;            
        END IF;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MPS1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MPS1`(IN i INT, OUT media FLOAT)
BEGIN		
        DECLARE p1, p2 float;        
			IF (SELECT IF (
				(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1) > 
					(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 1), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p1 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1);
			ELSE
				SET p1 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 1);
			END IF;
            
            IF (SELECT IF (
				(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1) > 
					(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 2), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p2 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1);
			ELSE
				SET p2 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 2);
			END IF;	

			SET media = (IFNULL(p1,0)+IFNULL(p2,0))/2.0;	
		
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MPS2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MPS2`(IN i INT, OUT media FLOAT)
BEGIN		
        DECLARE p3, p4 float;        
			IF (SELECT IF (
				(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2) > 
					(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 3), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p3 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2);
			ELSE
				SET p3 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 3);
			END IF;
            
            IF (SELECT IF (
				(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2) > 
					(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 4), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p4 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2);
			ELSE
				SET p4 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 4);
			END IF;	
            
			SET media = (IFNULL(p3,0)+IFNULL(p4,0))/2.0;	
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MT`(IN insc INT, OUT n FLOAT)
BEGIN
  
    DECLARE trabalhos_q_sairam, qnt_trabalhos int DEFAULT 0;
    DECLARE soma_trabalho float DEFAULT 0;
      

    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                              WHERE n.inscricao = insc AND d.tipo = 'T');

    SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                        WHERE i.inscricao = insc); 
      
    SET soma_trabalho = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                         INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'T');
  
    IF (trabalhos_q_sairam < qnt_trabalhos)	
    THEN

      SET n = IFNULL(soma_trabalho,0) / NULLIF(trabalhos_q_sairam,0);
    
    ELSE	
        	
		SET n = (
        SELECT 
			ROUND(SUM((nota.valor * detalhe.peso)) / SUM(detalhe.peso),1)
		FROM
			((ebloom.inscricao
			INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
			INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
		WHERE
			ebloom.inscricao.inscricao = insc
				AND ebloom.detalhe.tipo = 'T');

    END IF;
					 
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `PREVISAO_APROVACAO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `PREVISAO_APROVACAO`(IN exer int, OUT previsao float)
BEGIN
       
       DECLARE total_alunos int DEFAULT 0;
       DECLARE previsao_aprovacao float DEFAULT 0;
        
       #SET total_alunos = 5; 
       SET total_alunos = (SELECT COUNT(*) FROM matricula WHERE exercicio = exer);
       SET previsao_aprovacao = (SELECT SUM(prob_permanencia) FROM previsao_evasao INNER JOIN matricula m 
       ON previsao_evasao.matricula = m.matricula WHERE m.exercicio = exer);   
        
       SET previsao = NULLIF(previsao_aprovacao,0) / NULLIF(total_alunos,0);    

  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `PREVISAO_EVASAO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `PREVISAO_EVASAO`(IN exer int, OUT previsao float)
BEGIN
       
       DECLARE total_alunos int DEFAULT 0;
       DECLARE prev_evasao float DEFAULT 0;
       
       #SET total_alunos = 5;
       SET total_alunos = (SELECT COUNT(*) FROM matricula WHERE exercicio = exer);
       SET prev_evasao = (SELECT SUM(prob_evasao) FROM previsao_evasao INNER JOIN matricula m 
       ON previsao_evasao.matricula = m.matricula WHERE m.exercicio = exer);   
  
       SET previsao = NULLIF(prev_evasao,0) / NULLIF(total_alunos,0);    
  
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-08 23:18:08
