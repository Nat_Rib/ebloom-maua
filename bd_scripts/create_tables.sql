

set foreign_key_checks = 0;


CREATE TABLE `aluno` (
  `aluno` int NOT NULL AUTO_INCREMENT,
  `user` int DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nome` varchar(45) DEFAULT NULL,
  `sobrenome` varchar(45) DEFAULT NULL,
  `ra` varchar(45) NOT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`aluno`),
  KEY `fk_aluno_user1_idx` (`user`),
  CONSTRAINT `fk_aluno_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6509 DEFAULT CHARSET=utf8;



CREATE TABLE `assunto` (
  `assunto` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `descricao` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`assunto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `cronograma` (
  `cronograma` int NOT NULL AUTO_INCREMENT,
  `grade_curricular` int NOT NULL,
  `curso` int NOT NULL,
  `disciplina` int NOT NULL,
  `dia_semana` enum('DOMINGO','SEGUNDA','TERCA','QUARTA','QUINTA','SEXTA','SABADO') DEFAULT NULL,
  `horario` time DEFAULT NULL,
  PRIMARY KEY (`cronograma`,`grade_curricular`,`curso`,`disciplina`),
  KEY `fk_cronograma_grade_curricular1_idx` (`grade_curricular`,`curso`,`disciplina`),
  CONSTRAINT `fk_cronograma_grade_curricular1` FOREIGN KEY (`grade_curricular`, `curso`, `disciplina`) REFERENCES `grade_curricular` (`grade_curricular`, `curso`, `disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `curso` (
  `curso` int NOT NULL AUTO_INCREMENT COMMENT 'coordenador do curso',
  `nome` varchar(45) DEFAULT NULL,
  `qualidade` int DEFAULT NULL,
  `professor` int NOT NULL,
  PRIMARY KEY (`curso`,`professor`),
  KEY `fk_curso_professor1_idx` (`professor`),
  CONSTRAINT `fk_curso_professor1` FOREIGN KEY (`professor`) REFERENCES `professor` (`professor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;




CREATE TABLE `detalhe` (
  `detalhe` int NOT NULL AUTO_INCREMENT,
  `disciplina` int NOT NULL,
  `tipo` enum('T','P','S') NOT NULL,
  `numero` int NOT NULL,
  `peso` float DEFAULT NULL,
  `bimestre` int DEFAULT NULL,
  PRIMARY KEY (`detalhe`,`disciplina`),
  KEY `fk_detalhe_disciplina1_idx` (`disciplina`),
  CONSTRAINT `fk_detalhe_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3110 DEFAULT CHARSET=utf8;




CREATE TABLE `disciplina` (
  `disciplina` int NOT NULL AUTO_INCREMENT,
  `professor` int NOT NULL,
  `curso` int NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `moodle` varchar(500) DEFAULT NULL,
  `semestralidade` enum('AN','S1','S2') DEFAULT NULL,
  `qtdP` int DEFAULT NULL,
  `qtdT` int DEFAULT NULL,
  `kP` float DEFAULT NULL,
  `kT` float DEFAULT NULL,
  `ementa` varchar(500) DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `carga_horaria` int NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`disciplina`,`professor`,`curso`),
  KEY `fk_disciplina_professor1_idx` (`professor`),
  KEY `fk_disciplina_curso1_idx` (`curso`),
  CONSTRAINT `fk_disciplina_curso1` FOREIGN KEY (`curso`) REFERENCES `curso` (`curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disciplina_professor1` FOREIGN KEY (`professor`) REFERENCES `professor` (`professor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=572 DEFAULT CHARSET=utf8;




CREATE TABLE `estagio` (
  `estagio` int NOT NULL AUTO_INCREMENT,
  `aluno` int NOT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `empresa` varchar(45) DEFAULT NULL,
  `inicio` date NOT NULL,
  `termino` date DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`estagio`,`aluno`),
  KEY `fk_estagio_aluno1_idx` (`aluno`),
  CONSTRAINT `fk_estagio_aluno1` FOREIGN KEY (`aluno`) REFERENCES `aluno` (`aluno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `grade_curricular` (
  `grade_curricular` int NOT NULL AUTO_INCREMENT,
  `curso` int NOT NULL,
  `disciplina` int NOT NULL,
  `serie` int DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`grade_curricular`,`curso`,`disciplina`),
  KEY `fk_curso_has_disciplina_disciplina1_idx` (`disciplina`),
  KEY `fk_curso_has_disciplina_curso1_idx` (`curso`),
  CONSTRAINT `fk_curso_has_disciplina_curso1` FOREIGN KEY (`curso`) REFERENCES `curso` (`curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_curso_has_disciplina_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `inscricao` (
  `inscricao` int NOT NULL AUTO_INCREMENT,
  `disciplina` int NOT NULL,
  `matricula` int NOT NULL,
  `aluno` int NOT NULL,
  `curso` int NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `media_prova` float DEFAULT NULL,
  `media_trabalho` float DEFAULT NULL,
  `media_final` float DEFAULT NULL,
  `ranking_disciplina` int DEFAULT NULL,
  `ranking_disciplina_anterior` int DEFAULT NULL,
  `origem` int NOT NULL,
  `situacao_inicial` int NOT NULL,
  `situacao_final` int DEFAULT NULL,
  `grupo` int DEFAULT '1',
  `turma` int DEFAULT '1',
  `laboratorio` int DEFAULT '1',
  `monitor` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`inscricao`,`disciplina`,`matricula`,`aluno`,`curso`,`origem`,`situacao_inicial`),
  UNIQUE KEY `UK_inscricao_inscricao` (`inscricao`),
  KEY `fk_matriculado_disciplina1_idx` (`disciplina`),
  KEY `fk_matricula_situacao1_idx` (`origem`),
  KEY `fk_matricula_situacao2_idx` (`situacao_inicial`),
  KEY `fk_matricula_situacao3_idx` (`situacao_final`),
  KEY `fk_inscricao_matricula1_idx` (`matricula`,`aluno`,`curso`),
  CONSTRAINT `fk_inscricao_matricula1` FOREIGN KEY (`matricula`, `aluno`, `curso`) REFERENCES `matricula` (`matricula`, `aluno`, `curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_situacao1` FOREIGN KEY (`origem`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_situacao2` FOREIGN KEY (`situacao_inicial`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_situacao3` FOREIGN KEY (`situacao_final`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matriculado_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=119070 DEFAULT CHARSET=utf8;




CREATE TABLE `interesse` (
  `interesse` int NOT NULL AUTO_INCREMENT,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `assunto` int NOT NULL,
  `user` int NOT NULL,
  PRIMARY KEY (`interesse`,`assunto`,`user`),
  KEY `fk_interesse_assunto1_idx` (`assunto`),
  KEY `fk_interesse_user1_idx` (`user`),
  CONSTRAINT `fk_interesse_assunto1` FOREIGN KEY (`assunto`) REFERENCES `assunto` (`assunto`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_interesse_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `leciona` (
  `leciona` int NOT NULL AUTO_INCREMENT,
  `disciplina` int NOT NULL,
  `professor` int NOT NULL,
  `notificado` tinyint(4) DEFAULT '0',
  `status` int DEFAULT '0' COMMENT 'tipo de professor (coordenador, regular, integral)',
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`leciona`,`disciplina`,`professor`),
  KEY `fk_leciona_disciplina1_idx` (`disciplina`),
  KEY `fk_leciona_professor1` (`professor`),
  CONSTRAINT `fk_leciona_disciplina1` FOREIGN KEY (`disciplina`) REFERENCES `disciplina` (`disciplina`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_leciona_professor1` FOREIGN KEY (`professor`) REFERENCES `professor` (`professor`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=744 DEFAULT CHARSET=utf8;




CREATE TABLE `matricula` (
  `matricula` int NOT NULL AUTO_INCREMENT,
  `aluno` int NOT NULL,
  `curso` int NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `serie` int NOT NULL,
  `exercicio` year(4) DEFAULT NULL,
  `periodo` enum('DIURNO','NOTURNO','MATUTINO') DEFAULT NULL,
  `coeficiente_rendimento` float DEFAULT NULL,
  `ranking_curso` int DEFAULT NULL,
  `ranking_serie` int DEFAULT NULL,
  `origem` int NOT NULL,
  `situacao_inicial` int NOT NULL,
  `situacao_final` int DEFAULT NULL,
  PRIMARY KEY (`matricula`,`aluno`,`curso`,`origem`,`situacao_inicial`),
  UNIQUE KEY `UK_matricula_matricula` (`matricula`),
  KEY `fk_desempenho_aluno1_idx` (`aluno`),
  KEY `fk_desempenho_situacao1_idx` (`origem`),
  KEY `fk_desempenho_situacao2_idx` (`situacao_inicial`),
  KEY `fk_desempenho_situacao3_idx` (`situacao_final`),
  KEY `fk_matricula_curso1_idx` (`curso`),
  CONSTRAINT `fk_desempenho_aluno1` FOREIGN KEY (`aluno`) REFERENCES `aluno` (`aluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_desempenho_situacao1` FOREIGN KEY (`origem`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_desempenho_situacao2` FOREIGN KEY (`situacao_inicial`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_desempenho_situacao3` FOREIGN KEY (`situacao_final`) REFERENCES `situacao` (`situacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_curso1` FOREIGN KEY (`curso`) REFERENCES `curso` (`curso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15295 DEFAULT CHARSET=utf8;



CREATE TABLE `nota` (
  `nota` int NOT NULL AUTO_INCREMENT,
  `inscricao` int NOT NULL,
  `detalhe` int NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lancado` tinyint(4) NOT NULL,
  `valor` float DEFAULT NULL,
  PRIMARY KEY (`nota`,`inscricao`,`detalhe`),
  KEY `fk_nota_matricula1_idx` (`inscricao`),
  KEY `fk_nota_detalhe1_idx` (`detalhe`),
  CONSTRAINT `fk_nota_detalhe1` FOREIGN KEY (`detalhe`) REFERENCES `detalhe` (`detalhe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_nota_matricula1` FOREIGN KEY (`inscricao`) REFERENCES `inscricao` (`inscricao`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=898815 DEFAULT CHARSET=utf8;




CREATE TABLE `notificacao` (
  `notificacao` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `titulo` varchar(75) DEFAULT NULL,
  `mensagem` varchar(500) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notificacao`,`user`),
  KEY `fk_notificacao_mensagem1_idx` (`mensagem`),
  KEY `fk_notificacao_user1` (`user`),
  CONSTRAINT `fk_notificacao_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `previsao_disciplina` (
  `previsao` int NOT NULL AUTO_INCREMENT,
  `inscricao` int NOT NULL,
  `aprovacao` float DEFAULT NULL,
  `reprovacao` float DEFAULT NULL,
  `resultado` int DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`previsao`,`inscricao`),
  KEY `fk_inscricao_matricula2_idx` (`inscricao`),
  CONSTRAINT `fk_inscricao_matricula2_idx` FOREIGN KEY (`inscricao`) REFERENCES `inscricao` (`inscricao`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5613 DEFAULT CHARSET=utf8;




CREATE TABLE `previsao_evasao` (
  `evasao` int NOT NULL AUTO_INCREMENT,
  `matricula` int NOT NULL,
  `prob_permanencia` float DEFAULT NULL,
  `prob_evasao` float DEFAULT NULL,
  `resultado_evasao` int DEFAULT NULL,
  `prob_reprovacao` float DEFAULT NULL,
  `prob_promocao` float DEFAULT NULL,
  `resultado_reprovacao` int DEFAULT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`evasao`,`matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=1843 DEFAULT CHARSET=utf8;




CREATE TABLE `prof_users` (
  `email` text,
  `pwd` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `professor` (
  `professor` int NOT NULL AUTO_INCREMENT,
  `user` int DEFAULT NULL,
  `nome` varchar(45) NOT NULL,
  `sobrenome` varchar(45) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lattes` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`professor`),
  KEY `fk_professor_user1_idx` (`user`),
  CONSTRAINT `fk_professor_user1` FOREIGN KEY (`user`) REFERENCES `user` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;




CREATE TABLE `score_ml` (
  `score_ml` int NOT NULL AUTO_INCREMENT,
  `valor_score` float DEFAULT NULL,
  `prova` varchar(255) DEFAULT NULL,
  `trabalho` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`score_ml`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;



CREATE TABLE `situacao` (
  `situacao` int NOT NULL AUTO_INCREMENT,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campo` enum('MATRICULA','DISCIPLINA') DEFAULT NULL,
  `tipo` enum('ORIGEM','SITUACAO') DEFAULT NULL,
  `cod` varchar(5) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `observacao` varchar(1000) DEFAULT NULL,
  `aprovacao` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`situacao`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;


CREATE TABLE `user` (
  `user` int NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `pwd` varchar(45) NOT NULL,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo` enum('aluno','professor','admin') NOT NULL,
  `foto` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;

set foreign_key_checks = 1;
