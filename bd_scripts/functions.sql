CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`aprovacao_disciplina`(cod varchar(10), ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE total_alunos float DEFAULT 0;
	DECLARE total_alunos_aprovados float DEFAULT 0;
	DECLARE taxa_aprovacao float DEFAULT 0;
	SET total_alunos = (
		SELECT count(*) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat ON ins.matricula = mat.matricula
		INNER JOIN ebloom.disciplina as dis ON dis.disciplina = ins.disciplina 
		INNER JOIN ebloom.previsao_disciplina as prev ON prev.inscricao = ins.inscricao
		WHERE mat.exercicio = ano AND dis.codigo = cod
	);

	SET total_alunos_aprovados = (
		SELECT count(*) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat ON mat.matricula = ins.matricula
		INNER JOIN ebloom.disciplina as dis ON dis.disciplina = ins.disciplina
		INNER JOIN ebloom.previsao_disciplina as prev ON prev.inscricao = ins.inscricao
		WHERE ebloom.media_final(ins.inscricao) >= 6 AND dis.codigo = cod AND mat.exercicio = ano 
	);
	
	SET taxa_aprovacao = total_alunos_aprovados/NULLIF(total_alunos,0);

	RETURN taxa_aprovacao;
	
END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`coeficiente`(m int) RETURNS float
BEGIN
	DECLARE coeficiente float DEFAULT 0;
	CALL COEF(m, coeficiente);
	RETURN coeficiente;
END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`media_ano_bimestre`(d int, bimestre int, ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE media float DEFAULT 0;  
   	CALL MEDIA_HISTORICA(d,bimestre,ano, media);
	RETURN ROUND(media,1);

END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`media_final`(i int) RETURNS float
    DETERMINISTIC
BEGIN
		DECLARE media float DEFAULT 0;     
    CALL MEDIA_DISCIPLINA_2(i, media);          
		RETURN media;
    END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`media_prova`(i int) RETURNS float
    DETERMINISTIC
BEGIN
		DECLARE media float DEFAULT 0;
      
      IF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
         ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i)  = 'AN'
      
      THEN           
    	    CALL MPAN(i, media);

      ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i) = 'S1'
      THEN          
          CALL MPS1(i, media);

      ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i) = 'S2'

      THEN
          CALL MPS1(i, media);

      END IF;
          
		RETURN ROUND(media,1);
    END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`media_prova_parcial`(insc int) RETURNS float
    DETERMINISTIC
BEGIN
     SET @outvar = 0.0; 
    CALL ebloom.MEDIA_PROVA_PARCIAL(insc, @outvar);
    RETURN ROUND(@outvar,1);
END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`media_trabalho`(i int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE media float DEFAULT 0;  
   	CALL MT(i, media);
	RETURN ROUND(media,1);
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`media_trabalho_parcial`(insc int) RETURNS float
    DETERMINISTIC
BEGIN
      
      SET @outvar = 0.0;
      CALL ebloom.MEDIA_TRAB_PARCIAL(insc, @outvar);
      RETURN ROUND(@outvar,1);
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`previsao_aprovacao`(exer int) RETURNS float
    DETERMINISTIC
BEGIN
        DECLARE evasao float DEFAULT 0;  
        CALL  PREVISAO_APROVACAO(exer, evasao);
        RETURN ROUND(NULLIF(evasao,0),2);
    END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`previsao_aprovacao_disciplina`(cod VARCHAR(10), ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE total_alunos float DEFAULT 0;
	DECLARE total_alunos_aprovados float DEFAULT 0;
	DECLARE taxa_aprovacao float DEFAULT 0;
	SET total_alunos = (
		SELECT COUNT(*) FROM inscricao i INNER JOIN previsao_disciplina pd ON i.inscricao = pd.inscricao
		INNER JOIN disciplina d ON i.disciplina = d.disciplina INNER JOIN matricula m ON i.matricula = m.matricula 
		WHERE m.exercicio = ano AND d.codigo = cod
	);

	SET total_alunos_aprovados = (
		SELECT count(*) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.previsao_disciplina as prev ON prev.inscricao = ins.inscricao
		INNER JOIN ebloom.matricula as mat ON mat.matricula = ins.matricula
		INNER JOIN ebloom.disciplina as dis ON dis.disciplina = ins.disciplina
		WHERE prev.resultado = 0 AND dis.codigo = cod AND mat.exercicio = ano 
	);
	
	SET taxa_aprovacao = total_alunos_aprovados/NULLIF(total_alunos,0);

	RETURN taxa_aprovacao;
	
END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`previsao_evasao`(exer int) RETURNS float
    DETERMINISTIC
BEGIN
        DECLARE evasao float DEFAULT 0;  
        CALL  PREVISAO_EVASAO(exer, evasao);
        RETURN ROUND(NULLIF(evasao,0),2);
    END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`taxa_evasao_1serie_ano`(ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE matriculas_ano float DEFAULT 0;
	DECLARE desistentes float DEFAULT 0;
	DECLARE taxa_evasao float DEFAULT 0;
	
	SET matriculas_ano = (SELECT COUNT(*) from ebloom.matricula WHERE serie = 1 and exercicio=ano);

	SET desistentes = (SELECT COUNT(*) from ebloom.matricula as mat 
	INNER JOIN ebloom.situacao as sit ON mat.situacao_final = sit.situacao 
	WHERE serie = 1 and exercicio=ano and sit.nome in ("JUB","TRG","INT","CAN","CANC","DLG""DES","FAL","TFEE","NRM"));
	
	IF ((matriculas_ano) != 0)
	THEN
		SET taxa_evasao = 1 - ((matriculas_ano - desistentes)/(matriculas_ano));
		IF ((taxa_evasao) >= 0)
		THEN
			RETURN taxa_evasao;
		ELSE
			RETURN NULL;
		END IF;
	ELSE
		RETURN NULL;
	END IF;
END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`taxa_evasao_ano`(ano int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE matriculas_ano float DEFAULT 0;
	DECLARE matriculas_ano_anterior float DEFAULT 0;
	DECLARE egressos float DEFAULT 0;
	DECLARE ingressantes float DEFAULT 0;
	DECLARE taxa_evasao float DEFAULT 0;
	DECLARE formados float DEFAULT 0;
	DECLARE saida float DEFAULT 0;
	DECLARE matr_ausentes_diurno float DEFAULT 0;
	DECLARE matr_ausentes_noturno float DEFAULT 0;
	SET matriculas_ano = ebloom.total_alunos_ano(ano);
	SET matriculas_ano_anterior = ebloom.total_alunos_ano(ano-1);
	SET egressos = ebloom.total_alunos_saida_ano(ano-1);
	SET ingressantes = ebloom.total_alunos_novos_ano(ano);
	SET formados = ebloom.total_alunos_formados_ano(ano-1);
	SET matr_ausentes_diurno = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "DIURNO" AND mat.exercicio = ano-1
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano)) = 0
	);
	SET matr_ausentes_noturno = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "NOTURNO" AND mat.exercicio = (ano-1)
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano)) = 0
	);
	SET matriculas_ano_anterior = matriculas_ano_anterior + matr_ausentes_diurno + matr_ausentes_noturno;
	SET saida = egressos + formados;
	IF ((matriculas_ano_anterior - saida) != 0)
	THEN
		SET taxa_evasao = 1 - ((matriculas_ano - ingressantes)/(matriculas_ano_anterior - saida));
		IF ((taxa_evasao) >= 0)
		THEN
			RETURN taxa_evasao;
		ELSE
			RETURN NULL;
		END IF;
	ELSE
		RETURN NULL;
	END IF;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`total_alunos_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	SET total = (
		SELECT count(*) FROM ebloom.matricula WHERE exercicio = ano
	);
	RETURN total;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`total_alunos_aprovados_disciplina_atual`(idDis int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT COUNT(ins.inscricao) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis and ebloom.media_final(ins.inscricao) >= 6
		);
	RETURN media;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`total_alunos_disciplina_atual`(idDis int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT COUNT(ins.inscricao) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis
		);
	RETURN media;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`total_alunos_formados_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	DECLARE total_formados int DEFAULT 0;
	DECLARE total_fmdausentes int DEFAULT 0;
	DECLARE total_fmdausentesnoturno int DEFAULT 0;
	SET total_formados = (
		SELECT count(*) 
		FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and mat.situacao_final = "FMD"
	);
	SET total_fmdausentes = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "DIURNO" AND mat.exercicio = ano
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano+1)) = 0
	);
	SET total_fmdausentesnoturno = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit ON sit.situacao = mat.situacao_final and sit.nome = "PRO"
		WHERE mat.serie = 4 AND mat.periodo = "NOTURNO" AND mat.exercicio = (ano)
		AND (SELECT count(*) FROM ebloom.matricula as mat2 WHERE mat2.aluno = mat.aluno AND mat2.serie = 5 AND mat.exercicio = (ano+1)) = 0
	);

	SET total = total_formados + total_fmdausentes + total_fmdausentesnoturno;
	RETURN total;
END;


CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`total_alunos_novos_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	SET total = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit on sit.situacao = mat.origem and sit.nome IN ("VES","QLF","TRFE","NHAB")
		WHERE mat.exercicio = ano
	);
	RETURN total;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`total_alunos_reprovados_disciplina_atual`(idDis int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT COUNT(ins.inscricao) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis and ebloom.media_final(ins.inscricao) < 6
		);
	RETURN media;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`total_alunos_saida_ano`(ano int) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE total int DEFAULT 0;
	SET total = (
		SELECT count(*) FROM ebloom.matricula as mat
		INNER JOIN ebloom.situacao as sit on sit.situacao = mat.situacao_final and sit.nome IN ("JUB","TRG","INT","CAN","CANC","DLG""DES","FAL","TFEE","NRM")
		WHERE mat.exercicio = ano
	);
	RETURN total;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`val_media_disciplina_atual`(idDis int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT AVG(ebloom.media_final(ins.inscricao)) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio = ano
		WHERE ins.disciplina = idDis
		);
	RETURN media;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`val_media_disciplina_historico`(idDis int) RETURNS float
    DETERMINISTIC
BEGIN
	DECLARE ano int DEFAULT 0;
	DECLARE media float DEFAULT 0;
	SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = idDis
		);
	SET media = (
		SELECT AVG(ebloom.media_final(ins.inscricao)) FROM ebloom.inscricao as ins
		INNER JOIN ebloom.matricula as mat on mat.matricula = ins.matricula and mat.exercicio != ano
		WHERE ins.disciplina = idDis
		);
	RETURN media;
END;

CREATE DEFINER=`root`@`%` FUNCTION `ebloom`.`verificar_nota_lancada`(i int, d int) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	DECLARE ano,qtd int DEFAULT 0;
	SET ano = (SELECT mat.exercicio FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.inscricao = i);
	SET qtd = (
		SELECT count(*) 
		FROM ebloom.nota as nota
		INNER JOIN ebloom.inscricao as ins ON ins.inscricao = nota.inscricao
		INNER JOIN ebloom.matricula as mat ON mat.matricula = ins.matricula and mat.exercicio = ano
		WHERE nota.detalhe = d
	);
	
	IF (qtd > 0) THEN
	RETURN TRUE;
	ELSE 
	RETURN FALSE;
	END IF;


END;

