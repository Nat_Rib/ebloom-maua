CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`COEF`(IN m INT, OUT media FLOAT)
BEGIN		
		DECLARE qnt, disc, cargaHor, insc INT;
        DECLARE aux1, aux2 FLOAT;
        DECLARE n INT DEFAULT 0;
		DECLARE i INT DEFAULT 0;
        SET n = (SELECT count(inscricao) FROM ebloom.inscricao WHERE matricula = m);
		  
        SET media = 0;
        SET aux1 = 0;
        SET aux2 = 0;
        
        WHILE i<n DO
			SET insc = (SELECT inscricao FROM ebloom.inscricao WHERE matricula = m ORDER BY inscricao ASC LIMIT i,1);
                        
            IF (SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.matricula = m 
            ORDER BY ebloom.disciplina.disciplina LIMIT 1) = "S1"
            
            THEN
				SET @outvar = 0.0;
				CALL MFS1(insc,@outvar);
                SET cargaHor = (SELECT carga_horaria FROM ebloom.disciplina ORDER BY ebloom.disciplina.disciplina LIMIT 1);
                SET aux1 = @outvar*cargaHor;
                SET media = media + aux1;
                SET aux2 = aux2 + cargaHor;
                
				SET i = i + 1;
				SET aux1 = 0;
			
            ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.matricula = m 
            ORDER BY ebloom.disciplina.disciplina LIMIT 1) = "AN" 
			
            THEN
				SET @outvar = 0.0;
				CALL MFAN(insc,@outvar);  
                SET cargaHor = (SELECT carga_horaria FROM ebloom.disciplina ORDER BY ebloom.disciplina.disciplina LIMIT 1);
                SET aux1 = @outvar*cargaHor;
                SET media = media + aux1;
                SET aux2 = aux2 + cargaHor;
			
				SET i = i + 1;
				SET aux1 = 0;
                
			ELSEIF(SELECT semestralidade FROM ebloom.disciplina INNER JOIN ebloom.inscricao 
            ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.matricula = m 
            ORDER BY ebloom.disciplina.disciplina LIMIT 1) = "S2" 
			
            THEN
				SET @outvar = 0.0;
				CALL MFS2(insc,@outvar);  
                SET cargaHor = (SELECT carga_horaria FROM ebloom.disciplina ORDER BY ebloom.disciplina.disciplina LIMIT 1);
                SET aux1 = @outvar*cargaHor;
                SET media = media + aux1;
                SET aux2 = aux2 + cargaHor;
			
				SET i = i + 1;
				SET aux1 = 0;
			
			END IF;
   
		END WHILE;		
        	
        SET media = media/aux2;   
        
    END;


    CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MEDIA_ATUAL_BIMESTRE`(IN d int)
BEGIN
       DECLARE tipo_dis VARCHAR(20) DEFAULT 0;
       DECLARE ano int DEFAULT 0;
       SET tipo_dis = (SELECT semestralidade FROM disciplina WHERE disciplina = d);
       
       SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = d
		);
       
       IF(tipo_dis = "AN")
       THEN
			SELECT 
			ROUND(AVG(ebloom.media_ano_bimestre(d, 1,mat.exercicio)),1) as b1,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 2,mat.exercicio)),1) as b2,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 3,mat.exercicio)),1) as b3,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 4,mat.exercicio)),1) as b4
			FROM ebloom.matricula as mat
			INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
			INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
			WHERE mat.exercicio = ano;
       ELSE
       	   	SELECT 
		   	ROUND(ebloom.media_ano_bimestre(d, 1,mat.exercicio),1) as b1,
		   	ROUND(ebloom.media_ano_bimestre(d, 2,mat.exercicio),1) as b2
		   	FROM ebloom.matricula as mat
		   	INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
		   	INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
	       	WHERE mat.exercicio = ano;
	    END IF;
END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MEDIA_DISCIPLINA`(IN insc int, OUT media_final float)
BEGIN

        DECLARE semest varchar(2);
        DECLARE p1,s1,p2,s2,p3,p4,soma_prova,soma_trabalho,peso_prova,peso_trabalho,media_prova,media_trabalho,
                media_prova_parcial, media_trabalho_parcial float DEFAULT 0;
        DECLARE provas_q_sairam, trabalhos_q_sairam, qnt_provas, qnt_trabalhos int DEFAULT 0;
        
        SET semest = (SELECT d.semestralidade FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                      WHERE i.inscricao = insc);

        SET qnt_provas = (SELECT qtdP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET provas_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                               WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

        SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                  WHERE n.inscricao = insc AND d.tipo = 'T');

        SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);

        SET peso_prova = (SELECT kP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET peso_trabalho = (SELECT kT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);
        
        #SELECT semest, qnt_provas, provas_q_sairam, qnt_trabalhos, trabalhos_q_sairam;

        IF(qnt_provas = 0)
        THEN       
             #SELECT 'trabalho';    
             CALL MT(insc, @mt);
             SET media_final = ROUND(@mt,1);
        ELSE
  
          IF (provas_q_sairam < qnt_provas)
          THEN
                  
                  SET soma_prova = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                                    INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'P');
  
                  SET soma_trabalho = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                                       INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'T');
  
                  SET provas_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                         WHERE n.inscricao = insc AND (d.tipo = 'P'));
  
                  SET media_prova_parcial = soma_prova / provas_q_sairam;
                  SET media_trabalho_parcial = soma_trabalho / trabalhos_q_sairam;
                  
                  #SELECT "parcial", media_prova_parcial, media_trabalho_parcial, peso_prova, peso_trabalho;
  
                  SET media_final = (IFNULL(media_prova_parcial,0) * peso_prova + IFNULL(media_trabalho_parcial,0) * peso_trabalho) 
                                      / NULLIF((peso_prova + peso_trabalho),0);
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 6
          THEN           
                  # Quando todas as notas foram lançadas
                  #SELECT semest;
    
                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p2;
                  END IF;
      
                  SET s2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT s2;
                  
                  SET p3 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 3 AND i.inscricao = insc);
      
                  #SELECT p3;
      
                  SET p4 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 4 AND i.inscricao = insc);
      
                  #SELECT p4;
      
                  IF(s2 > p3)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p3;
                  END IF;
      
                  IF(s2 > p4)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p4;
                  END IF;
                  
                  #SELECT soma_prova;
                  SET media_prova = soma_prova / 10;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
              #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
  
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 3
          THEN 
                  #SELECT "AN";

                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p2;
                  END IF;
  
                  SET media_prova = soma_prova / 2;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
                  SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
                  #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
                
  
          ELSEIF (semest) = "S1" OR (semest) = "S2"
          THEN
              #SELECT "S1 ou S1";
              
              SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT s1;
              
              SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT p1;
  
              SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 2 AND i.inscricao = insc);
  
              #SELECT p2;
  
              IF(s1 > p1)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p1;
              END IF;
  
              IF(s1 > p2)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p2;
              END IF;
  
              #SELECT soma_prova;
  
              SET media_prova = soma_prova / 2;
              CALL MT(insc, @mt);
              SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0);
              
              #SELECT "total - S1 ou S2", media_trabalho, media_prova, peso_prova, peso_trabalho;
                 
          END IF;  

      END IF;
     
  END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MEDIA_DISCIPLINA_2`(IN insc int, OUT media_final float)
BEGIN

        DECLARE semest varchar(2);
        DECLARE p1,s1,p2,s2,p3,p4,soma_prova,soma_trabalho,peso_prova,peso_trabalho,media_prova,media_trabalho,
                media_prova_parcial, media_trabalho_parcial float DEFAULT 0;
        DECLARE provas_q_sairam, trabalhos_q_sairam, qnt_provas, qnt_trabalhos int DEFAULT 0;
        
        SET semest = (SELECT d.semestralidade FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                      WHERE i.inscricao = insc);

        SET qnt_provas = (SELECT qtdP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET provas_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                               WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

        SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                  WHERE n.inscricao = insc AND d.tipo = 'T');

        SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);

        SET peso_prova = (SELECT kP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                          WHERE i.inscricao = insc);

        SET peso_trabalho = (SELECT kT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);
        
        #SELECT semest, qnt_provas, provas_q_sairam, qnt_trabalhos, trabalhos_q_sairam;

        IF(qnt_provas = 0)
        THEN       
             #SELECT 'MATERIAS FULL TRABALHO';    
             CALL MT(insc, @mt);
             SET media_final = ROUND(@mt,1);
        ELSE
  
            IF (provas_q_sairam < qnt_provas)
            THEN
                  
                IF (semest) = "AN" AND (qnt_provas) = 6
                THEN           
                  #PARCIAL DE MATERIA ANUAL COM 6 PROVAS

                    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                              WHERE n.inscricao = insc AND d.tipo = 'T');

                    #SELECT trabalhos_q_sairam;

                    SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                                           ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

                    #SELECT provas_q_sairam;
                     
                    SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                                d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT s1;
                  
                    SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT p1;
      
                    SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 2 AND i.inscricao = insc);
      
                    #SELECT p1, p2, s1;
                IF (NOT ISNULL(p1) OR NOT ISNULL (s1))
                THEN
      
                    IF(COALESCE(s1,0) > COALESCE(p1,0))              
                    THEN     
                       # SELECT "entrou";         
                        SET soma_prova = soma_prova +  s1;
                       # SELECT soma_prova;
                    ELSE
                        #SELECT "entrou errado";
                        SET soma_prova = soma_prova +  p1;
                    END IF;
                END IF;


                IF (NOT ISNULL(p2) OR NOT ISNULL (s1))
                THEN
                    IF(COALESCE(s1,0) > COALESCE(p2,0))              
                    THEN              
                        SET soma_prova = soma_prova +  s1;
                    ELSE
                        SET soma_prova = soma_prova +  p2;
                    END IF;
                END IF;
      
                    SET s2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                                d.numero = 2 AND i.inscricao = insc);
      
                    #SELECT s2;
                  
                    SET p3 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 3 AND i.inscricao = insc);
      
                    #SELECT p3;
      
                    SET p4 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 4 AND i.inscricao = insc);
      
                    #SELECT p4;
                IF (NOT ISNULL(p3) OR NOT ISNULL (s2))
                THEN
                    IF(COALESCE(s2,0) > COALESCE(p3,0))              
                    THEN    
                                   
                        SET soma_prova = soma_prova +  s2;
                    ELSE
                       # SELECT "entrou certo na p3";
                        SET soma_prova = soma_prova +  p3;
                       # SELECT soma_prova;
                    END IF;
                END IF;
                    
              IF (NOT ISNULL(p4) OR NOT ISNULL (s2))
                THEN

                    IF(COALESCE(s2,0) > COALESCE(p4,0))              
                    THEN               
                        SET soma_prova = soma_prova +  s2;
                    ELSE
                        SET soma_prova = soma_prova + p4;
                    END IF;

             END IF;
                
                    #SELECT soma_prova;   
                    
                    IF(NOT ISNULL(s1))
                    THEN
                        SET provas_q_sairam = provas_q_sairam - 1;
                    END IF;

                    IF(NOT ISNULL(s1 AND s2))
                    THEN
                        SET provas_q_sairam = provas_q_sairam - 2;
                    END IF;

                    #SELECT provas_q_sairam; 
                    #SELECT soma_prova; 
                                   
                    SET media_final = NULLIF(soma_prova,0) / NULLIF(provas_q_sairam,0);
                    CALL MEDIA_TRAB_PARCIAL(insc, @mt);
                    SET media_trabalho = ROUND(@mt,1);

                    #SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                        #NULLIF((peso_prova + peso_trabalho),0); 
  
                #SELECT "total - AN - PARCIAL",media_prova, media_trabalho, peso_prova, peso_trabalho;
  
  
                ELSEIF (semest) = "AN" AND ((qnt_provas) = 3 OR (qnt_provas) = 2)
                THEN                    
                    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                              WHERE n.inscricao = insc AND d.tipo = 'T');

                    SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                                           ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S')); 
                    #SELECT "AN";

                    SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                             INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                             d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT s1;
                  
                    SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                                INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                                d.numero = 1 AND i.inscricao = insc);
      
                    #SELECT p1;
      
                    SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                    #SELECT p2;


                IF (NOT ISNULL(s1) OR NOT ISNULL (p1))
                  THEN
                    IF(IFNULL(s1,0) > IFNULL(p1,0))              
                    THEN 
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                        SET soma_prova = soma_prova + p1;
                    END IF;
                 END IF;

                IF (NOT ISNULL(s1) OR NOT ISNULL (p2))
                  THEN
                    IF(IFNULL(s1,0) > IFNULL(p2,0))              
                    THEN   
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                      SET soma_prova = soma_prova + p2;
                    END IF;
                 END IF;

                    #SELECT soma_prova, provas_q_sairam;
                    SET media_prova = NULLIF(soma_prova,0) / NULLIF(provas_q_sairam,0);
                    CALL MEDIA_TRAB_PARCIAL(insc, @mt);
                    SET media_trabalho = ROUND(@mt,1);

                    
  
                    SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                       NULLIF((peso_prova + peso_trabalho),0); 
    
                    #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
                
  
                ELSEIF (semest) = "S1" OR (semest) = "S2"
                THEN
                    #SELECT "S1 ou S1";

                    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                              WHERE n.inscricao = insc AND d.tipo = 'T');

                    SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                                           ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S')); 
              
                    SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
  
                    #SELECT s1;
                
                    SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
    
                    #SELECT p1;
    
                    SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
    
                    #SELECT p2;
  

                    IF (NOT ISNULL(s1) OR NOT ISNULL (p1))
                      THEN
                    IF(IFNULL(s1,0) > IFNULL(p1,0))              
                    THEN   
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                        SET soma_prova = soma_prova + p1;
                    END IF;
                  END IF;


                  IF (NOT ISNULL(s1) OR NOT ISNULL (p2))
                    THEN
                    IF(IFNULL(s1,0) > IFNULL(p2,0))              
                    THEN    
                        
                        SET soma_prova = soma_prova + s1;
                    ELSE
                        SET soma_prova = soma_prova + p2;
                    END IF;
                  END IF;
  
                    #SELECT soma_prova, provas_q_sairam;
  
                    SET media_final = NULLIF(soma_prova,0) / NULLIF(provas_q_sairam,0);
                    CALL MEDIA_TRAB_PARCIAL(insc, @mt);
                    SET media_trabalho = ROUND(@mt,1);
        
                    #SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                        #NULLIF((peso_prova + peso_trabalho),0);
                    
                    #SELECT "total - S1 ou S2", media_trabalho, media_prova, peso_prova, peso_trabalho;
                 
            END IF;  

          #JA SAIU TODAS AS NOTAS
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 6
          THEN           
                  # Quando todas as notas foram lançadas
                  #SELECT semest;
    
                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + 2 * s1;
                  ELSE
                      SET soma_prova = soma_prova + 2 * p2;
                  END IF;
      
                  SET s2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT s2;
                  
                  SET p3 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 3 AND i.inscricao = insc);
      
                  #SELECT p3;
      
                  SET p4 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 4 AND i.inscricao = insc);
      
                  #SELECT p4;
      
                  IF(s2 > p3)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p3;
                  END IF;
      
                  IF(s2 > p4)              
                  THEN                
                      SET soma_prova = soma_prova + 3 * s2;
                  ELSE
                      SET soma_prova = soma_prova + 3 * p4;
                  END IF;
                  
                  #SELECT soma_prova;
                  SET media_prova = soma_prova / 10;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
              #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
  
  
          ELSEIF (semest) = "AN" AND (qnt_provas) = 3
          THEN 
                  #SELECT "AN";

                  SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT s1;
                  
                  SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 1 AND i.inscricao = insc);
      
                  #SELECT p1;
      
                  SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                            INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                            d.numero = 2 AND i.inscricao = insc);
      
                  #SELECT p2;
      
                  IF(s1 > p1)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p1;
                  END IF;
      
                  IF(s1 > p2)              
                  THEN                
                      SET soma_prova = soma_prova + s1;
                  ELSE
                      SET soma_prova = soma_prova + p2;
                  END IF;
  
                  SET media_prova = soma_prova / 2;
                  CALL MT(insc, @mt);
                  SET media_trabalho = ROUND(@mt,1);
  
                  SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0); 
  
                  #SELECT "total - AN",media_prova, media_trabalho, peso_prova, peso_trabalho;
                
  
          ELSEIF (semest) = "S1" OR (semest) = "S2"
          THEN
              #SELECT "S1 ou S1";
              
              SET s1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'S' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT s1;
              
              SET p1 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 1 AND i.inscricao = insc);
  
              #SELECT p1;
  
              SET p2 = (SELECT valor FROM nota n INNER JOIN inscricao i ON n.inscricao = i.inscricao 
                        INNER JOIN detalhe d ON n.detalhe = d.detalhe WHERE d.tipo = 'P' AND 
                        d.numero = 2 AND i.inscricao = insc);
  
              #SELECT p2;
  
              IF(s1 > p1)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p1;
              END IF;
  
              IF(s1 > p2)              
              THEN                
                  SET soma_prova = soma_prova + s1;
              ELSE
                  SET soma_prova = soma_prova + p2;
              END IF;
  
              #SELECT soma_prova;
  
              SET media_prova = soma_prova / 2;
              CALL MT(insc, @mt);
              SET media_trabalho = ROUND(@mt,1);
  
              SET media_final = (IFNULL(media_prova,0) * peso_prova + IFNULL(media_trabalho,0) * peso_trabalho) /
                                NULLIF((peso_prova + peso_trabalho),0);
              
              #SELECT "total - S1 ou S2", media_trabalho, media_prova, peso_prova, peso_trabalho;
                 
          END IF;  

      END IF;
     
  END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MEDIA_HISTORICA`(IN d int, IN b int, IN a year, OUT media_hist float)
BEGIN
        DECLARE numero_alunos int DEFAULT 0;
        DECLARE soma_provas, soma_trabalhos, peso_trabalho, peso_prova, media_hist_prova, media_hist_trabalho float DEFAULT 0;

        SET numero_alunos = (SELECT COUNT(inscricao) FROM inscricao INNER JOIN matricula m ON inscricao.matricula = m.matricula AND 
                             inscricao.aluno = m.aluno AND inscricao.curso = m.curso WHERE disciplina = d AND m.exercicio = a);

        #SELECT numero_alunos;
                            
        SET peso_trabalho = (SELECT kT FROM disciplina WHERE disciplina = d);

        #SELECT peso_trabalho;
       
        SET peso_prova = (SELECT kP FROM disciplina WHERE disciplina = d);

        #SELECT peso_prova;
      
        SET soma_provas = (SELECT SUM(nota.valor) FROM nota INNER JOIN inscricao ON nota.inscricao = inscricao.inscricao                        
                           INNER JOIN detalhe ON nota.detalhe = detalhe.detalhe INNER JOIN matricula ON
                           inscricao.matricula = matricula.matricula WHERE detalhe.disciplina = d AND
                           detalhe.tipo = "P" AND detalhe.bimestre = b AND matricula.exercicio = a); 

        #SELECT soma_provas;
                          
        SET soma_trabalhos = (SELECT SUM(nota.valor)/count(DISTINCT det.numero) FROM nota 
							INNER JOIN inscricao as ins ON nota.inscricao = ins.inscricao                        
							INNER JOIN detalhe as det ON nota.detalhe = det.detalhe 
							INNER JOIN matricula as mat ON ins.matricula = mat.matricula 
							WHERE det.disciplina = d AND det.tipo = "T" AND det.bimestre = b AND mat.exercicio = a); 

            
        #SELECT soma_trabalhos;

      
        SET media_hist_prova = soma_provas/numero_alunos;

        #SELECT media_hist_prova;
       
        SET media_hist_trabalho = soma_trabalhos/numero_alunos;

        #SELECT media_hist_trabalho;
              
       	SET media_hist = ((media_hist_prova * peso_prova) + (media_hist_trabalho * peso_trabalho)) / (peso_prova + peso_trabalho); 

        #SELECT media_hist;
         
  END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MEDIA_HISTORICA_BIMESTRE`(IN d int)
BEGIN
       DECLARE tipo_dis VARCHAR(20) DEFAULT 0;
       DECLARE ano int DEFAULT 0;
       SET tipo_dis = (SELECT semestralidade FROM disciplina WHERE disciplina = d);
       
       SET ano = (
		SELECT max(mat.exercicio) 
		FROM ebloom.matricula as mat, ebloom.inscricao as ins WHERE mat.matricula = ins.matricula and ins.disciplina = d
		);
       
       IF(tipo_dis = "AN")
       THEN
			SELECT 
			ROUND(AVG(ebloom.media_ano_bimestre(d, 1,mat.exercicio)),1) as b1,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 2,mat.exercicio)),1) as b2,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 3,mat.exercicio)),1) as b3,
			ROUND(AVG(ebloom.media_ano_bimestre(d, 4,mat.exercicio)),1) as b4
			FROM ebloom.matricula as mat
			INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
			INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
			WHERE mat.exercicio = ano-1;
       ELSE
       	   	SELECT 
		   	ROUND(ebloom.media_ano_bimestre(d, 1,mat.exercicio),1) as b1,
		   	ROUND(ebloom.media_ano_bimestre(d, 2,mat.exercicio),1) as b2
		   	FROM ebloom.matricula as mat
		   	INNER JOIN ebloom.inscricao as ins ON ins.matricula = mat.matricula
		   	INNER JOIN ebloom.disciplina as dis ON ins.disciplina = dis.disciplina and dis.disciplina = d
	       	WHERE mat.exercicio = ano-1;
	    END IF;
END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MEDIA_PROVA_PARCIAL`(IN insc int, OUT media_prova_parcial float)
BEGIN

  DECLARE qnt_provas, provas_q_sairam int DEFAULT 0;
  DECLARE mpp, soma_prova, peso_prova float DEFAULT 0;

  SET qnt_provas = (SELECT qtdP FROM disciplina d INNER JOIN inscricao i
                    ON d.disciplina = i.disciplina WHERE i.inscricao = insc);

  SET provas_q_sairam = (SELECT COUNT(valor)FROM nota n INNER JOIN detalhe d
                         ON n.detalhe = d.detalhe WHERE n.inscricao = insc AND (d.tipo = 'P' OR d.tipo = 'S'));

  SET soma_prova = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i
        ON nota.inscricao = i.inscricao INNER JOIN detalhe d
        ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'P');

  SET peso_prova = (SELECT kP FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                    WHERE i.inscricao = insc);

  SET media_prova_parcial = soma_prova / provas_q_sairam;

  #SET media_prova_parcial = (mpp * peso_prova) / NULLIF(peso_prova, 0);

#SELECT soma_provas, provas_q_sairam, peso_prova;
#SELECT soma_trabalhos, provas_q_sairam, peso_trabalho;

END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MEDIA_TRAB_PARCIAL`(IN insc int, OUT media_trab_parcial float)
BEGIN

  DECLARE qnt_trabalhos,
          trabalhos_q_sairam int DEFAULT 0;
  DECLARE mtp,
          soma_trabalho, peso_trabalho float DEFAULT 0;

  SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                                  WHERE n.inscricao = insc AND d.tipo = 'T');

  SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                             WHERE i.inscricao = insc);
  SET peso_trabalho = (SELECT kT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                       WHERE i.inscricao = insc);

  SET soma_trabalho = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                       INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'T');


  SET media_trab_parcial = soma_trabalho / trabalhos_q_sairam;

  #SET media_trab_parcial = IFNULL((mtp * peso_trabalho),0) / NULLIF(peso_trabalho, 0);

#SELECT soma_provas, provas_q_sairam, peso_prova;
#SELECT soma_trabalhos, provas_q_sairam, peso_trabalho;

END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MFAN`(IN i INT, OUT mf FLOAT)
BEGIN	
	
		DECLARE pesoProva, pesoTrab FLOAT;
        
        SET @outvar1 = 0.0;
        CALL MPAN(i,@outvar1);
        SET @outvar2 = 0.0;
        CALL MT(i,@outvar2);
		
        SET pesoProva = (SELECT kP 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
                        
        SET pesoTrab = (SELECT kT 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
        
        SET mf = (IFNULL(pesoProva,0) * IFNULL(@outvar1,0) + IFNULL(pesoTrab,0) * IFNULL(@outvar2,0))/(IFNULL(pesoProva,0) + IFNULL(pesoTrab,0));
        
    END;

CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MFS1`(IN i INT, OUT mf FLOAT)
BEGIN	
	
		DECLARE pesoProva, pesoTrab FLOAT;
        
        SET @outvar1 = 0.0;
        CALL MPS1(i,@outvar1);
        SET @outvar2 = 0.0;
        CALL MT(i,@outvar2);
		
        SET pesoProva = (SELECT kP 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
                        
        SET pesoTrab = (SELECT kT 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
	
        SET mf = (IFNULL(pesoProva,0) * IFNULL(@outvar1,0) + IFNULL(pesoTrab,0) * IFNULL(@outvar2,0))/(IFNULL(pesoProva,0) + IFNULL(pesoTrab,0));
        
    END;

CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MFS2`(IN i INT, OUT mf FLOAT)
BEGIN	
	
		DECLARE pesoProva, pesoTrab FLOAT;
        
        SET @outvar1 = 0.0;
        CALL MPS1(i,@outvar1);
        SET @outvar2 = 0.0;
        CALL MT(i,@outvar2);        
		
        SET pesoProva = (SELECT kP 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
                        
        SET pesoTrab = (SELECT kT 
						FROM ebloom.disciplina
						INNER JOIN ebloom.inscricao ON ebloom.disciplina.disciplina = ebloom.inscricao.disciplina WHERE ebloom.inscricao.inscricao = i);
        
        SET mf = (IFNULL(pesoProva,0) * IFNULL(@outvar1,0) + IFNULL(pesoTrab,0) * IFNULL(@outvar2,0))/(IFNULL(pesoProva,0) + IFNULL(pesoTrab,0));
        
    END;

CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MPAN`(IN i INT, OUT media FLOAT)
BEGIN
		IF(SELECT qtdP FROM disciplina INNER JOIN inscricao ON disciplina.disciplina = inscricao.disciplina 
       WHERE inscricao = i) = 6
        THEN
			
			SET @outvar1 = 0.0;
			CALL MPS1(i,@outvar1);
			SET @outvar2 = 0.0;
			CALL MPS2(i,@outvar2);
			SET media = (2 * @outvar1 + 3 * @outvar2)/5;
            
		ELSEIF(SELECT qtdP FROM disciplina INNER JOIN inscricao ON disciplina.disciplina = inscricao.disciplina 
            WHERE inscricao = i) = 3
        THEN
			SET @outvar1 = 0.0;
            CALL MPS1(i,@outvar1);
            SET media = @outvar1;            
        END IF;
  END;

CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MPS1`(IN i INT, OUT media FLOAT)
BEGIN		
        DECLARE p1, p2 float;        
			IF (SELECT IF (
				(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1) > 
					(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 1), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p1 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1);
			ELSE
				SET p1 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 1);
			END IF;
            
            IF (SELECT IF (
				(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1) > 
					(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 2), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p2 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 1);
			ELSE
				SET p2 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 2);
			END IF;	

			SET media = (IFNULL(p1,0)+IFNULL(p2,0))/2.0;	
		
    END;

CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MPS2`(IN i INT, OUT media FLOAT)
BEGIN		
        DECLARE p3, p4 float;        
			IF (SELECT IF (
				(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2) > 
					(SELECT valor FROM ((ebloom.inscricao INNER JOIN ebloom.nota 
					ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
					INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 3), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p3 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2);
			ELSE
				SET p3 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 3);
			END IF;
            
            IF (SELECT IF (
				(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2) > 
					(SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 4), 
				"TRUE", "FALSE")) = "TRUE"
			THEN 
				SET p4 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'S' AND detalhe.numero = 2);
			ELSE
				SET p4 = (SELECT valor FROM
                    ((ebloom.inscricao
                    INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
                    INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
					WHERE inscricao.inscricao = i AND detalhe.tipo = 'P' AND detalhe.numero = 4);
			END IF;	
            
			SET media = (IFNULL(p3,0)+IFNULL(p4,0))/2.0;	
    END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`MT`(IN insc INT, OUT n FLOAT)
BEGIN
  
    DECLARE trabalhos_q_sairam, qnt_trabalhos int DEFAULT 0;
    DECLARE soma_trabalho float DEFAULT 0;
      

    SET trabalhos_q_sairam = (SELECT COUNT(valor) FROM nota n INNER JOIN detalhe d ON n.detalhe = d.detalhe 
                              WHERE n.inscricao = insc AND d.tipo = 'T');

    SET qnt_trabalhos = (SELECT qtdT FROM disciplina d INNER JOIN inscricao i ON d.disciplina = i.disciplina
                        WHERE i.inscricao = insc); 
      
    SET soma_trabalho = (SELECT SUM(valor) FROM nota INNER JOIN inscricao i ON nota.inscricao = i.inscricao
                         INNER JOIN detalhe d ON nota.detalhe = d.detalhe WHERE i.inscricao = insc AND tipo = 'T');
  
    IF (trabalhos_q_sairam < qnt_trabalhos)	
    THEN

      SET n = IFNULL(soma_trabalho,0) / NULLIF(trabalhos_q_sairam,0);
    
    ELSE	
        	
		SET n = (
        SELECT 
			ROUND(SUM((nota.valor * detalhe.peso)) / SUM(detalhe.peso),1)
		FROM
			((ebloom.inscricao
			INNER JOIN ebloom.nota ON ebloom.inscricao.inscricao = ebloom.nota.inscricao)
			INNER JOIN ebloom.detalhe ON ebloom.nota.detalhe = ebloom.detalhe.detalhe)
		WHERE
			ebloom.inscricao.inscricao = insc
				AND ebloom.detalhe.tipo = 'T');

    END IF;
					 
    END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`PREVISAO_APROVACAO`(IN exer int, OUT previsao float)
BEGIN
       
       DECLARE total_alunos int DEFAULT 0;
       DECLARE previsao_aprovacao float DEFAULT 0;
        
       #SET total_alunos = 5; 
       SET total_alunos = (SELECT COUNT(*) FROM matricula WHERE exercicio = exer);
       SET previsao_aprovacao = (SELECT SUM(prob_permanencia) FROM previsao_evasao INNER JOIN matricula m 
       ON previsao_evasao.matricula = m.matricula WHERE m.exercicio = exer);   
        
       SET previsao = NULLIF(previsao_aprovacao,0) / NULLIF(total_alunos,0);    

  END;


CREATE DEFINER=`root`@`%` PROCEDURE `ebloom`.`PREVISAO_EVASAO`(IN exer int, OUT previsao float)
BEGIN
       
       DECLARE total_alunos int DEFAULT 0;
       DECLARE prev_evasao float DEFAULT 0;
       
       #SET total_alunos = 5;
       SET total_alunos = (SELECT COUNT(*) FROM matricula WHERE exercicio = exer);
       SET prev_evasao = (SELECT SUM(prob_evasao) FROM previsao_evasao INNER JOIN matricula m 
       ON previsao_evasao.matricula = m.matricula WHERE m.exercicio = exer);   
  
       SET previsao = NULLIF(prev_evasao,0) / NULLIF(total_alunos,0);    
  
  END;

