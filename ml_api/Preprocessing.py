import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.model_selection import train_test_split

class MultiColumnLabelEncoder:
    def __init__(self, columns = None):
        self.columns = columns # list of column to encode
        self.le = {}
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        '''
        Transforms columns of X specified in self.columns using
        LabelEncoder(). If no columns specified, transforms all
        columns in X.
        '''
        
        output = X.copy()
        
        if self.columns is not None:
            for col in self.columns:
                if col not in self.le: self.le[col] = LabelEncoder()#.fit(output[col])
                #output[col] = output[col].map(lambda s: 'other' if s not in self.le[col].classes_ else s)
                output[col] = self.le[col].fit_transform(output[col])
        else:
            for colname, col in output.iteritems():
                if colname not in self.le: self.le[colname] = LabelEncoder()#.fit(col)
                #col = col.map(lambda s: 'other' if s not in self.le[colname].classes_ else s)
                output[colname] = self.le[colname].fit_transform(col)
        
        return output
    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X)


class DataDisciplina:
    # instance variables
    labels = ["StFin","StFinDisc","MT","MP","MF"]
    provas = ["P1","P2","P3","P4"]
    trabalhos = ["T1","T2","T3","T4","T5","T6","T7","T8","T9","T10","T11","T12","T13","T14","T15"]

    def __init__(self, dataset=None, prova=None, trabalho=None):
        # object variables
        self.data = dataset.copy()

        if prova == 'PS1':
            self.prova = 'P2'
        elif prova == 'PS2':
            self.prova = 'P4'
        else:
            self.prova = prova

        self.trabalho = trabalho
        
        self.features = None
        self.label = None
        
        self.train = None
        self.test = None

        self.ohe = None
        self.mle = None
        self.le = None
        self.sc = None

        self.setup()

    def set_data(self, dataset):
        self.data = dataset
    def get_data(self):
        return self.data


    def setup(self):
        self.clean()

        # rever para evasao!
        self.data = self.data.drop([
            'Esc',
            'CdDis',
            'CurDis',
            'SerDis',
            'SemDisc',
            'CritDisc'
        ],axis=1)

        fp = self.get_provas()
        ft = self.get_trabalhos()

        (features,evasao_data,disc_data) = self.split(self.data)

        cat_data = features.select_dtypes(include=[np.object])
        self.mle = MultiColumnLabelEncoder().fit(cat_data)
        self.ohe = {
            'categoria': OneHotEncoder(sparse=False,handle_unknown='ignore').fit( self.mle.transform(cat_data) ),
            'evasao': OneHotEncoder(sparse=False).fit( self.mle.transform(evasao_data) ),
            'disciplina': OneHotEncoder(sparse=False).fit( self.mle.transform(disc_data) )
        }
        self.le = LabelEncoder().fit(self.data['StFinDisc'])

        features_drop = self.data.drop(fp,axis=1).drop(ft,axis=1)
        num = features_drop.drop(self.labels,axis=1).select_dtypes(include=[np.int64,np.float64])
        self.sc = StandardScaler().fit(num)
        predict_data = self.prepare(features_drop)

        self.predict = {
            'features': predict_data[0],
            'label': {
                'evasao': predict_data[1],
                'disciplina': predict_data[2]
            },
            'categoria': {
                'evasao': predict_data[3],
                'disciplina': predict_data[4]
            }
        }

        train_set, test_set = train_test_split(self.data, test_size=0.3, random_state=42)
        train_set_drop = train_set.drop(fp,axis=1).drop(ft,axis=1)
        test_set_drop = test_set.drop(fp,axis=1).drop(ft,axis=1)
        #num = train_set_drop.drop(self.labels,axis=1).select_dtypes(include=[np.int64,np.float64])
        #self.sc = StandardScaler().fit(num)
        train_data = self.prepare(train_set_drop)
        test_data = self.prepare(test_set_drop)

        self.train = {
            'features': train_data[0],
            'label': {
                'evasao': train_data[1],
                'disciplina': train_data[2]
            },
            'categoria': {
                'evasao': train_data[3],
                'disciplina': train_data[4]
            }
        }

        self.test = {
            'features': test_data[0],
            'label': {
                'evasao': test_data[1],
                'disciplina': test_data[2]
            },
            'categoria': {
                'evasao': test_data[3],
                'disciplina': test_data[4]
            }
        }

    def setup_predict(self, data):
        self.clean(data)

        # rever para evasao!
        self.data = self.data.drop([
            'Esc',
            'CdDis',
            'CurDis',
            'SerDis',
            'SemDisc',
            'CritDisc'
        ],axis=1)

        fp = self.get_provas()
        ft = self.get_trabalhos()
        
        features_drop = self.data.drop(self.labels, axis=1).drop(fp,axis=1).drop(ft,axis=1)
        num_features = self.sc.transform(features_drop.select_dtypes(include=[np.int64,np.float64]))
        cat_features = self.ohe['categoria'].transform( self.mle.transform(features_drop.select_dtypes(include=[np.object])) )
        prepared_features = np.concatenate((num_features,cat_features),axis = 1)

        return prepared_features

    def clean(self, dataset=None):
        if dataset is not None: self.data = dataset.copy()
        ignore_cols = ['T16','DtStDisc','DtSitCur','Exer','RA','G','T','L','KT']
        nan_cols = ['MF','MP','MT','QtTrabalhos','KP',
                    'P1','P2','PS1','P3','P4','PS2',
                    'T1','T2','T3','T4','T5','T6','T7','T8','T9','T10','T11','T12','T13','T14','T15']
        
        self.data = self.data.drop(ignore_cols, axis=1)

        self.data['Cur'] = self.data['Cur'].str.replace(r'[0-9]+', '')
        self.data['CurDis'] = self.data['Cur'].str.replace(r'[0-9]+', '')


        self.data['KP'] = np.where(self.data['KP'] > 1, self.data['KP']/10, self.data['KP'])
        self.data['KP'] = np.where(self.data['CritDisc'] == 'A1', 0, self.data['KP'])
        self.data['QtTrabalhos'] = np.where(self.data['CritDisc'] == 'A1', 
                                    np.where(self.data['QtTrabalhos'].isnull(), 1,self.data['QtTrabalhos']), 
                                    self.data['QtTrabalhos'])

        for col in nan_cols:
            self.data[col].fillna(0,inplace=True)
            self.data[col] = np.where(self.data[col] < 0, 0, self.data[col])

        
        self.data['KP'] = np.where(self.data['QtTrabalhos'] == 0, 1, self.data['KP'])

        # colocar subs
        self.data['P1'] = np.where(self.data['PS1'] > self.data['P1'], self.data['PS1'], self.data['P1'])
        self.data['P2'] = np.where(self.data['PS1'] > self.data['P2'], self.data['PS1'], self.data['P2'])
        self.data['P3'] = np.where(self.data['PS2'] > self.data['P3'], self.data['PS2'], self.data['P3'])
        self.data['P4'] = np.where(self.data['PS2'] > self.data['P4'], self.data['PS2'], self.data['P4'])
        self.data = self.data.drop(['PS1','PS2'], axis=1)

        # adcionar o fator KP e KT nas notas
        for p in self.provas:
            self.data[p] = self.data[p]*self.data['KP']
        for t in self.trabalhos:
            self.data[t] = self.data[t]*(1-self.data['KP'])

        self.data = self.data.drop(['KP'], axis=1)
            
        self.data['StFin'].replace('TRG', 'EVASAO', inplace=True)
        self.data['StFin'].replace('INT', 'EVASAO', inplace=True)
        self.data['StFin'].replace('CAN', 'EVASAO', inplace=True)

        self.data['StFinDisc'].replace('AP1', 'AP', inplace=True)
        self.data['StFinDisc'].replace('RP1', 'DP', inplace=True)
        self.data['StFinDisc'].replace('TRC', 'DP', inplace=True)
        self.data['StFinDisc'].fillna('DP',inplace=True)
        
        return self.data.copy()

    def split(self, dataset=None):
        split_data = dataset.copy()
        
        yStFin = pd.DataFrame({ "StFin": split_data["StFin"]})
        yStFinDisc = pd.DataFrame({ "StFinDisc": split_data["StFinDisc"]})
        #yMT = pd.DataFrame({ "MT": data["MT"]})
        #yMP = pd.DataFrame({ "MP": data["MP"]})
        #yMF = pd.DataFrame({ "MF": data["MF"]})

        features = split_data.drop(self.labels, axis=1)
        return (features,yStFin,yStFinDisc)#,yMT,yMP,yMF)

    def prepare(self, data):
        (features,StFin,StFinDisc) = self.split(data)

        num_features = self.sc.transform(features.select_dtypes(include=[np.int64,np.float64]))
        cat_features = self.ohe['categoria'].transform( self.mle.transform(features.select_dtypes(include=[np.object])) )
        prepared_features = np.concatenate((num_features,cat_features),axis = 1)
        
        leStFin = self.mle.transform(StFin.select_dtypes(include=[np.object]))
        yStFin = self.ohe['evasao'].transform( leStFin )

        leStFinDisc = self.mle.transform(StFinDisc.select_dtypes(include=[np.object]))
        yStFinDisc = self.ohe['disciplina'].transform( leStFinDisc )

        return (prepared_features,yStFin,yStFinDisc,leStFin,leStFinDisc)

    def get_nodes(self, feature,label):
        return (feature.shape[1],label.shape[1])

    def get_provas(self):
        pFeatures = list(self.provas)
        if (self.prova == None): return self.provas
        for p in self.provas:
            pFeatures.remove(p)
            if (self.prova == p): break
        return pFeatures

    def get_trabalhos(self):
        tFeatures = list(self.trabalhos)
        if (self.trabalho == None): return self.trabalhos
        for t in self.trabalhos:
            tFeatures.remove(t)
            if (self.trabalho == t): break
        return tFeatures


class DataEvasao:
    def __init__(self, dataset=None):
        # object variables
        self.data = dataset.copy()
        self.clean()
        self.data_evasao = self.data.copy()
        
        self.features = None
        self.label = None
        self.train = None
        self.test = None
        self.ohe = None
        self.mle = None
        self.le = None
        self.sc = None

    def clean(self):
        self.data = self.data[self.data['Esc'] == 'EEM']
        self.data = self.data.drop([
            'Esc','DtSitCur','CdDis','CurDis','PerDis','G','T','L','OriDisc','StIniDisc',
            'StFinDisc','DtStDisc','SemDisc','CritDisc','QtTrabalhos','KT','KP',
            'P1','P2','PS1','P3','P4','PS2',
            'T1','T2','T3','T4','T5','T6','T7','T8','T9','T10','T11','T12','T13','T14','T15','T16',
            'MT','MP','MF'
        ], axis=1)
        self.data = self.data.drop_duplicates(["RA","Exer","SerDis"])
        self.data = self.data.set_index(["RA","Exer"])

    def add_disciplina(self, disciplina, data, resultado, score_modelo):
        score = np.full_like(resultado[disciplina]['resultado'], 
                         score_modelo['acertos']['score'], 
                         dtype=np.double)
        r = pd.concat([
            pd.DataFrame({'RA': data['RA']}),
            pd.DataFrame({'Exer': data['Exer']}),
            pd.DataFrame(
                resultado[disciplina]['previsao'], 
                columns=['aprovacao_'+disciplina,'dp_'+disciplina]),
            pd.DataFrame(
                resultado[disciplina]['resultado'], 
                columns=['resultado_'+disciplina]),
            pd.DataFrame({'score_'+disciplina: score})
        ], axis=1)
        r = r.set_index(["RA","Exer"])
        self.data_evasao = self.data_evasao.join(r)

    def set_serie(self, serie):
        self.data_evasao = self.data.loc[self.data['SerDis'] == serie]
        self.data_evasao = self.data_evasao.drop(['SerDis'], axis=1)
        return self.data_evasao

    def reset_dataset(self):
        self.data_evasao = self.data.copy()
        return self.data_evasao

    def setup(self):
        self.data_evasao = self.data_evasao.fillna(0)
        self.data_evasao['StFin'].replace('TRG', 'EVASAO', inplace=True)
        self.data_evasao['StFin'].replace('INT', 'EVASAO', inplace=True)
        self.data_evasao['StFin'].replace('CAN', 'EVASAO', inplace=True)

        (features,educacional,admin) = self.split(self.data_evasao)

        cat_data = features.select_dtypes(include=[np.object])
        self.mle = MultiColumnLabelEncoder().fit(cat_data)
        self.le = {
            'reprovacao': LabelEncoder().fit(educacional['StFin']),
            'evasao': LabelEncoder().fit(admin['StFin'])
        }
        self.ohe = {
            'categoria': OneHotEncoder(sparse=False,handle_unknown='ignore').fit( self.mle.transform(cat_data) ),
            'reprovacao': OneHotEncoder(sparse=False,handle_unknown='ignore').fit( self.le['reprovacao'].transform(educacional).reshape((-1,1)) ),
            'evasao': OneHotEncoder(sparse=False,handle_unknown='ignore').fit( self.le['evasao'].transform(admin).reshape((-1,1)) )
        }
        num = features.select_dtypes(include=[np.int64,np.float64])
        self.sc = StandardScaler().fit(num)

        predict_data = self.prepare(self.data_evasao)

        self.predict = {
            'features': predict_data[0],
            'label': {
                'reprovacao': predict_data[1],
                'evasao': predict_data[2]
            },
            'categoria': {
                'reprovacao': predict_data[3],
                'evasao': predict_data[4]
            }
        }

        train_set, test_set = train_test_split(self.data_evasao, test_size=0.3, random_state=42)

        train_data = self.prepare(train_set)
        test_data = self.prepare(test_set)

        self.train = {
            'features': train_data[0],
            'label': {
                'reprovacao': train_data[1],
                'evasao': train_data[2]
            },
            'categoria': {
                'reprovacao': train_data[3],
                'evasao': train_data[4]
            }
        }

        self.test = {
            'features': test_data[0],
            'label': {
                'reprovacao': test_data[1],
                'evasao': test_data[2]
            },
            'categoria': {
                'reprovacao': test_data[3],
                'evasao': test_data[4]
            }
        }


    def setup_predict(self, dataset):
        pred_data = dataset.copy()
        pred_data = pred_data.fillna(0)

        (features,_,_) = self.split(pred_data)
        num_features = self.sc.transform(features.select_dtypes(include=[np.int64,np.float64]))
        cat_features = self.ohe['categoria'].transform( self.mle.transform(features.select_dtypes(include=[np.object])) )
        prepared_features = np.concatenate((num_features,cat_features),axis = 1)

        return prepared_features


    def split(self, data):
        features = data.drop('StFin', axis=1)
        st_fin = pd.DataFrame({ 'StFin': data['StFin']})
        
        admin = st_fin.copy()
        educacional = st_fin.copy()
        educacional['StFin'].replace('EVASAO', 'RET', inplace=True)
        admin['StFin'].replace('RET', 'FICOU', inplace=True)
        admin['StFin'].replace('PRO', 'FICOU', inplace=True)
        return (features,educacional,admin)

    def prepare(self, data):
        (features,educacional,admin) = self.split(data)

        num_features = self.sc.transform(features.select_dtypes(include=[np.int64,np.float64]))
        cat_features = self.ohe['categoria'].transform( self.mle.transform(features.select_dtypes(include=[np.object])) )
        prepared_features = np.concatenate((num_features,cat_features),axis = 1)

        label_le_edu = self.le['reprovacao'].transform(educacional.select_dtypes(include=[np.object]))
        label_edu = self.ohe['reprovacao'].transform( label_le_edu.reshape((-1,1)) )
        
        label_le_admin = self.le['evasao'].transform(admin.select_dtypes(include=[np.object]))
        label_admin = self.ohe['evasao'].transform( label_le_admin.reshape((-1,1)) )
        
        return (prepared_features,label_edu,label_admin,label_le_edu,label_le_admin)

    def get_nodes(self, feature,label):
        return (feature.shape[1],label.shape[1])


