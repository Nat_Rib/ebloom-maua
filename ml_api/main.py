from flask import Flask
from MachineLearning import MLDisciplina, MLEvasao
from Preprocessing import DataEvasao
import time
import os
import numpy as np
import pandas as pd
app = Flask(__name__)

# executar o treino depois de responder (async)
# app = Flask("after_response")
# AfterResponse(app)
# @app.after_response
# def say_hi():
#     print("hi")


# REMOVER -> RECEBER DA BD
def load_data(data):
    dirname = os.path.dirname(__file__)
    fullpath = os.path.join(dirname, 'data')
    csv_path = os.path.join(fullpath, data)
    return pd.read_csv(csv_path,sep=';',decimal=',')
# REMOVER -> RECEBER DA BD


@app.route("/train")
def train():

    # REMOVER -> RECEBER DA BD
    data2015 = load_data('notas2015.csv')
    data2016 = load_data('notas2016.csv')
    data2017 = load_data('notas2017.csv')
    dataset = pd.concat([data2015,data2016,data2017],axis=0,sort=False)
    dataset["MF"] = dataset["MP"].copy()
    dataset["MP"] = dataset["MT"].copy()
    dataset["MT"] = dataset["T16"].copy()
    dataset["T16"] = np.nan
    # REMOVER -> RECEBER DA BD

    series = [1,2]
    provas = ['academico','P1','P2','PS1','P3','P4','PS2']
    trabalhos = ['academico','T1','T2','T3','T4','T5','T6','T7','T8','T9','T10','T11','T12','T13','T14','T15']

    for serie in series:
        tmp = dataset.loc[dataset['SerDis'] == serie]
        disciplinas = tmp.loc[tmp['Esc'] == 'EEM']['CdDis'].unique()
        for prova in provas:
            for trabalho in trabalhos:
                ml_serie = MLEvasao(serie, dataset)
                ml = {}
                result = {}
                for disc in disciplinas:
                    print ("\n\n" + disc + ": ("+ prova + "," + trabalho + ") \n")
                    if disc == 'EFB204': continue
                    dataDisc = dataset.loc[dataset['CdDis'] == disc]
                    # limitacao do modelo
                    if len(dataDisc.index) < 1500: continue
                    filteredDisc = dataset.loc[dataset['CdDis'] == disc].reset_index(drop=True)
                    ml[disc] = MLDisciplina(dataDisc, prova,trabalho)
                    ml[disc].train(0)
                    ml[disc].score(0)
                    ml[disc].save(disc)     
                    # preparar o dataset para evasao
                    result[disc] = ml[disc].predict()
                    ml_serie.data.add_disciplina( disc,filteredDisc,result,ml[disc].score_result )
                # treinar evasao e reprovacao
                ml_serie.setup()
                ml_serie.train('reprovacao',0)
                ml_serie.score('reprovacao',0)
                ml_serie.train('evasao',0)
                ml_serie.score('evasao',0)
                ml_serie.save(prova, trabalho)

    return '{"success":true}'
    

@app.route("/predict")
def predict():

    # REMOVER -> RECEBER DA BD
    data2015 = load_data('notas2015.csv')
    data2016 = load_data('notas2016.csv')
    data2017 = load_data('notas2017.csv')
    dataset = pd.concat([data2015,data2016,data2017],axis=0,sort=False)
    dataset["MF"] = dataset["MP"].copy()
    dataset["MP"] = dataset["MT"].copy()
    dataset["MT"] = dataset["T16"].copy()
    dataset["T16"] = np.nan
    # REMOVER -> RECEBER DA BD

    # REMOVER -> RECEBER DA BD
    pred_data = load_data('notas2018.csv')
    pred_data["MF"] = pred_data["MP"].copy()
    pred_data["MP"] = pred_data["MT"].copy()
    pred_data["MT"] = pred_data["T16"].copy()
    pred_data["T16"] = np.nan
    # REMOVER -> RECEBER DA BD

    series = [1,2]
    provas = ['P1','P2','PS1','P3','P4','PS2']
    trabalhos = ['T1','T2','T3','T4','T5','T6','T7','T8','T9','T10','T11','T12','T13','T14','T15']

    for serie in series:
        disc_modelos = next(os.walk('models/disciplina'))[1]
        tmp = dataset.loc[dataset['SerDis'] == serie]
        disciplinas = tmp.loc[tmp['Esc'] == 'EEM']['CdDis'].unique()
        st = 0
        sp = 0
        result = {}
        ml_serie = MLEvasao(serie, dataset)
        for disc in disciplinas:
            print(disc) 
            if not (disc in disc_modelos): continue
            try:
                main_data = dataset.loc[dataset['CdDis'] == disc]
                data_disc = pred_data.loc[pred_data['CdDis'] == disc]
                filteredDisc = pred_data.loc[pred_data['CdDis'] == disc].reset_index(drop=True)

                size = len(data_disc['QtTrabalhos'].value_counts())
                trabalho = 'academico'
                if (size > 0) :
                    trab = data_disc['QtTrabalhos'].value_counts().index[0]
                    for t in range(int(trab)):
                        if data_disc['T'+str(t+1)].value_counts().sum() <= 0: break
                        trabalho = 'T'+str(t+1)
                        if st < (t): st = (t+1)

                prova = 'academico'
                index = -1
                for p in provas:
                    index += 1
                    if data_disc[p].value_counts().sum() <= 0: break
                    prova = p
                    if sp < (index): sp = (index)

                ml = MLDisciplina(main_data,prova,trabalho)
                ml.load(disc)            
                result[disc] = ml.predict(pred_data)
                print(result) # SAVE ON DB
                ml.score(0)
                ml_serie.data.add_disciplina( disc,filteredDisc,result,ml.score_result )
            except:
                print('ERROR')
                pass

        ml_serie.setup()
        ml_serie.load(provas[sp], trabalhos[st])
        reprovacao = ml_serie.predict(ml_serie.data.data_evasao,'reprovacao')
        evasao = ml_serie.predict(ml_serie.data.data_evasao,'evasao')
        print(reprovacao) # SAVE ON DB
        print(evasao) # SAVE ON DB

    return '{"success":true}'