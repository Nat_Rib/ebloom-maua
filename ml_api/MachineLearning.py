# default imports
import numpy as np
import pandas as pd
import os

# sklearn import
#from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.metrics import accuracy_score

# keras import
from keras.models import Sequential
from keras.layers import Dense, Dropout
from sklearn.metrics import confusion_matrix

from Preprocessing import MultiColumnLabelEncoder, DataDisciplina, DataEvasao

from keras.models import load_model


# ML Class based on Keras Sequential Model
# This class is a wraper for the model, with methods for
#   - training
#   - predicting
#   - saving
#   - loading
#   - get model score
#   - setup the data (clean data)

class MLDisciplina:
    def __init__(self, dataset=None, prova=None, trabalho=None):
        self.data = DataDisciplina(dataset, prova, trabalho)
        (self.input_nodes,self.output_nodes) = self.data.get_nodes( self.data.train['features'], self.data.train['label']['disciplina'])
        self.model = self.build_model()

    # Machine Learning Model
    def build_model(self):
        classifier = Sequential()
        classifier.add(Dense(units = 6, kernel_initializer = 'random_uniform', activation = 'softmax', input_dim = self.input_nodes))
        classifier.add(Dropout(rate = 0.1))
        classifier.add(Dense(units = 3, kernel_initializer = 'random_uniform', activation = 'softmax'))
        classifier.add(Dropout(rate = 0.1))
        classifier.add(Dense(units = 3, kernel_initializer = 'random_uniform', activation = 'softmax'))
        classifier.add(Dense(units = self.output_nodes, kernel_initializer = 'random_uniform', activation = 'sigmoid'))
        classifier.compile(optimizer = 'rmsprop', loss = 'binary_crossentropy', metrics = ['accuracy'])
        return classifier


    # Training
    def train(self, verbose=1):
        # disciplina vs evaso ?? 
        features = self.data.train['features']
        label = self.data.train['label']['disciplina']
        #label = self.data.train['label']['disciplina']
        return self.model.fit(features, label, batch_size = 64, epochs = 100, shuffle=True, verbose=verbose)

    def score(self, verbose=1):
        if (self.data == None): return None

        features = self.data.test['features']
        label = self.data.test['label']['disciplina']

        pred = self.model.predict(features)
        pred_result = np.argmax(pred, axis=1)
        label_result = self.data.test['categoria']['disciplina']

        label_category = self.data.le.inverse_transform(label_result['StFinDisc'])
        pred_category = self.data.le.inverse_transform(pred_result)
        qtd_acertos = accuracy_score(label_result, pred_result, normalize=False)
        perc_acertos = accuracy_score(label_result, pred_result)
        loss, score = self.model.evaluate(features, label)
        
        # confusion matrix
        cm = confusion_matrix(label_category, pred_category,labels=self.data.le.classes_)
        (cm_tap,cm_fap,cm_fdp,cm_tdp) = cm.ravel()
        cm_total = float(cm[:,:].sum())
        cm_accuracy = (cm_tap + cm_tdp) / cm_total
        cm_error_rate = (cm_fap + cm_fdp) / cm_total

        cm_tap_rate = float(cm_tap) / cm[0,:].sum() if cm[0,:].sum() != 0 else 0
        cm_tdp_rate = float(cm_tdp) / cm[1,:].sum() if cm[1,:].sum() != 0 else 0
        cm_fap_rate = float(cm_fap) / cm[1,:].sum() if cm[1,:].sum() != 0 else 0
        cm_fdp_rate = float(cm_fdp) / cm[0,:].sum() if cm[0,:].sum() != 0 else 0
        cm_ap_precision = float(cm_tap) / cm[:,0].sum() if cm[:,0].sum() != 0 else 0
        cm_dp_precision = float(cm_tdp) / cm[:,1].sum() if cm[:,1].sum() != 0 else 0

        self.score_result = {
            'category': {
                'pred': pred_category,
                'label': label_category
            },
            'acertos': {
                'quantidade': qtd_acertos,
                'percentual': perc_acertos,
                'loss': loss,
                'score': score
            },
            'confusion_matrix': {
                'cm': cm,
                'percentual_acertos': cm_accuracy,
                'percentual_erros': cm_error_rate,
                'aprovado': {
                    'acertos': cm_tap_rate, # quando eh e preve q eh
                    'erros': cm_fap_rate, # quando nao eh e preve q eh
                    'precisao': cm_ap_precision
                },
                'reprovado': {
                    'acertos': cm_tdp_rate, # quando eh e preve q eh
                    'erros': cm_fdp_rate, # quando nao eh e preve q eh
                    'precisao': cm_dp_precision
                }
            }
        }
        if (verbose == 1): self.pprint(self.score_result)
        return self.score_result

    # Predicting
    def predict(self, data=None):
        if data is None:
            predict_data = self.data.predict['features']
        else:
            predict_data = self.data.setup_predict(data)

        pred = self.model.predict(predict_data)
        pred_result = np.argmax(pred, axis=1)
        pred_category = self.data.le.inverse_transform(pred_result)
        result = {
            'previsao': pred,
            'resultado': pred_result,
            'categoria': pred_category
        }
        return result

    # Model persistance
    def save(self, disc):
        path = 'models/disciplina'
        prova = self.data.prova
        trabalho = self.data.trabalho
        dirname = os.path.dirname(__file__)
        fullpath = os.path.join(dirname, path)
        if not os.path.exists(fullpath): os.mkdir(fullpath)
        fullpath = os.path.join(fullpath, disc)
        if not os.path.exists(fullpath): os.mkdir(fullpath)
        fullpath = os.path.join(fullpath, prova)
        if not os.path.exists(fullpath): os.mkdir(fullpath)
        self.model.save(fullpath+'/'+trabalho+'.h5')
    
    def load(self, disc):
        dirname = os.path.dirname(__file__)
        prova = self.data.prova
        trabalho = self.data.trabalho
        path = 'models/disciplina/' + disc + '/' + prova + '/' + trabalho
        fullpath = os.path.join(dirname, path)
        print fullpath
        self.model = load_model(fullpath + '.h5')

    def pprint(self, score):
        print("\nDataset:\n")
        print(pd.DataFrame({'TESTE': score['category']['label'] }).apply(pd.value_counts).T)
        print
        print(pd.DataFrame({'PRED': score['category']['pred'] }).apply(pd.value_counts).T)
        print
        print ("\nGeral:")
        print ("Quantidade de acertos:\t" + str(score['acertos']['quantidade']))
        print ("Taxa de acertos: \t" + str(score['acertos']['percentual']))
        print ("Loss: \t\t\t" + str(score['acertos']['loss']))
        print ("Score: \t\t\t" + str(score['acertos']['score']))
        print ("\nConfusion Matrix:\n")
        print (score['confusion_matrix']['cm'])
        print ("\nTaxa de acertos:\t" + str(score['confusion_matrix']['percentual_acertos']))
        print ("Taxa de erros: \t\t" + str(score['confusion_matrix']['percentual_erros']))
        print ("\nTaxa de Acertos:")
        print ("Aprovado \t(AP1): \t" + str(score['confusion_matrix']['aprovado']['acertos']))
        print ("Reprovado/DP \t(RP1): \t" + str(score['confusion_matrix']['reprovado']['acertos']))
        print ("\nTaxa de Erros:")
        print ("Aprovado \t(AP1): \t" + str(score['confusion_matrix']['aprovado']['erros']))
        print ("Reprovado/DP \t(RP1): \t" + str(score['confusion_matrix']['reprovado']['erros']))
        print ("\nPrecisao:")
        print ("Aprovado \t(AP1): \t" + str(score['confusion_matrix']['aprovado']['precisao']))
        print ("Reprovado/DP \t(RP1): \t" + str(score['confusion_matrix']['reprovado']['precisao']))






class MLEvasao:
    def __init__(self, ano, dataset=None):
        self.ano = ano
        self.data = DataEvasao(dataset)
        self.data.set_serie(ano)

    def setup(self):
        self.data.setup()
        rep_nodes = self.data.get_nodes( self.data.train['features'], self.data.train['label']['reprovacao'])
        evasao_nodes = self.data.get_nodes( self.data.train['features'], self.data.train['label']['evasao'])

        self.model = {
            'reprovacao': self.build_model_reprovacao( rep_nodes[0],rep_nodes[1] ),
            'evasao': self.build_model_evasao( evasao_nodes[0],evasao_nodes[1] )
        }

    # Machine Learning Model
    def build_model_reprovacao(self,input_nodes,output_nodes):
        classifier = Sequential()
        classifier.add(Dense(units = 50, kernel_initializer = 'random_uniform', activation = 'softmax', input_dim = input_nodes))
        classifier.add(Dropout(rate = 0.1))
        classifier.add(Dense(units = 50, kernel_initializer = 'random_uniform', activation = 'softmax'))
        classifier.add(Dense(units = 25, kernel_initializer = 'random_uniform', activation = 'softmax'))
        classifier.add(Dense(units = output_nodes, kernel_initializer = 'random_uniform', activation = 'sigmoid'))
        classifier.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])
        return classifier

    def build_model_evasao(self,input_nodes,output_nodes):
        classifier = Sequential()
        classifier.add(Dense(units = 50, kernel_initializer = 'random_uniform', activation = 'softmax', input_dim = input_nodes))
        classifier.add(Dense(units = output_nodes, kernel_initializer = 'random_uniform', activation = 'sigmoid'))
        classifier.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])
        return classifier


    # Training
    def train(self, model=None, verbose=1):
        if model != 'reprovacao' and model != 'evasao': return None
        epoch = 150
        #if model == 'reprovacao': epoch = 250

        features = self.data.train['features']
        label = self.data.train['label'][model]
        return self.model[model].fit(features, label, batch_size = 64, epochs = epoch, shuffle=True, verbose=verbose)

    def score(self, model=None, verbose=1):
        if (self.data == None): return None

        if model == 'reprovacao':
            cat1 = 'promocao'
            cat2 = 'reprovacao'
        elif model == 'evasao':
            cat1 = 'evasao'
            cat2 = 'permanencia'
        else:
            return None

        features = self.data.test['features']
        label = self.data.test['label'][model]

        pred = self.model[model].predict(features)
        pred_result = np.argmax(pred, axis=1)
        label_result = self.data.test['categoria'][model]

        label_category = self.data.le[model].inverse_transform(label_result)
        pred_category = self.data.le[model].inverse_transform(pred_result)
        qtd_acertos = accuracy_score(label_result, pred_result, normalize=False)
        perc_acertos = accuracy_score(label_result, pred_result)
        loss, score = self.model[model].evaluate(features, label)
        
        # confusion matrix
        cm = confusion_matrix(label_category, pred_category,labels=self.data.le[model].classes_)
        (cm_tap,cm_fap,cm_fdp,cm_tdp) = cm.ravel()
        cm_total = float(cm[:,:].sum())
        cm_accuracy = (cm_tap + cm_tdp) / cm_total
        cm_error_rate = (cm_fap + cm_fdp) / cm_total

        cm_tap_rate = float(cm_tap) / cm[0,:].sum() if cm[0,:].sum() != 0 else 0
        cm_tdp_rate = float(cm_tdp) / cm[1,:].sum() if cm[1,:].sum() != 0 else 0
        cm_fap_rate = float(cm_fap) / cm[1,:].sum() if cm[1,:].sum() != 0 else 0
        cm_fdp_rate = float(cm_fdp) / cm[0,:].sum() if cm[0,:].sum() != 0 else 0
        cm_ap_precision = float(cm_tap) / cm[:,0].sum() if cm[:,0].sum() != 0 else 0
        cm_dp_precision = float(cm_tdp) / cm[:,1].sum() if cm[:,1].sum() != 0 else 0

        self.score_result = {
            'category': {
                'pred': pred_category,
                'label': label_category
            },
            'acertos': {
                'quantidade': qtd_acertos,
                'percentual': perc_acertos,
                'loss': loss,
                'score': score
            },
            'confusion_matrix': {
                'cm': cm,
                'percentual_acertos': cm_accuracy,
                'percentual_erros': cm_error_rate,
                cat1: {
                    'acertos': cm_tap_rate, # quando eh e preve q eh
                    'erros': cm_fap_rate, # quando nao eh e preve q eh
                    'precisao': cm_ap_precision
                },
                cat2: {
                    'acertos': cm_tdp_rate, # quando eh e preve q eh
                    'erros': cm_fdp_rate, # quando nao eh e preve q eh
                    'precisao': cm_dp_precision
                }
            }
        }
        if (verbose == 1): self.pprint(self.score_result,cat1,cat2)
        return self.score_result

    # Predicting
    def predict(self, data=None, model=None):
        if model == None: return None
        if data is None:
            predict_data = self.data.predict['features']
        else:
            predict_data = self.data.setup_predict(data)

        pred = self.model[model].predict(predict_data)
        pred_result = np.argmax(pred, axis=1)
        pred_category = self.data.le[model].inverse_transform(pred_result)
        result = {
            'previsao': pred,
            'resultado': pred_result,
            'categoria': pred_category
        }
        return result

    # Model persistance
    def save(self, prova, trabalho):
        dirname = os.path.dirname(__file__)
        ano = str(self.ano)
        path = 'models/evasao'
        fullpath = os.path.join(dirname, path)
        if not os.path.exists(fullpath): os.mkdir(fullpath)
        fullpath = os.path.join(fullpath, ano)
        if not os.path.exists(fullpath): os.mkdir(fullpath)
        fullpath = os.path.join(fullpath, prova)
        if not os.path.exists(fullpath): os.mkdir(fullpath)
        fullpath = os.path.join(fullpath, trabalho)
        if not os.path.exists(fullpath): os.mkdir(fullpath)
        self.model['reprovacao'].save(fullpath+'/reprovacao.h5')
        self.model['evasao'].save(fullpath+'/evasao.h5')
    
    def load(self, prova, trabalho):
        dirname = os.path.dirname(__file__)
        ano = str(self.ano)
        path = 'models/evasao/' + ano + '/' + prova + '/' + trabalho
        fullpath = os.path.join(dirname, path)
        self.model['reprovacao'] = load_model(fullpath + '/reprovacao.h5')
        self.model['evasao'] = load_model(fullpath + '/evasao.h5')

    def pprint(self, score, cat1, cat2):
        print("\nDataset:\n")
        print(pd.DataFrame({'TESTE': score['category']['label'] }).apply(pd.value_counts).T)
        print
        print(pd.DataFrame({'PREVISAO': score['category']['pred'] }).apply(pd.value_counts).T)
        print
        print ("\nGeral:")
        print ("Quantidade de acertos:\t" + str(score['acertos']['quantidade']))
        print ("Taxa de acertos: \t" + str(score['acertos']['percentual']))
        print ("Loss: \t\t\t" + str(score['acertos']['loss']))
        print ("Score: \t\t\t" + str(score['acertos']['score']))
        print ("\nConfusion Matrix:\n")
        print (score['confusion_matrix']['cm'])
        print ("\nTaxa de acertos:\t" + str(score['confusion_matrix']['percentual_acertos']))
        print ("Taxa de erros: \t\t" + str(score['confusion_matrix']['percentual_erros']))
        print ("\nTaxa de Acertos:")
        print (cat1+": \t" + str(score['confusion_matrix'][cat1]['acertos']))
        print (cat2+": \t" + str(score['confusion_matrix'][cat2]['acertos']))
        print ("\nTaxa de Erros:")
        print (cat1+": \t" + str(score['confusion_matrix'][cat1]['erros']))
        print (cat2+": \t" + str(score['confusion_matrix'][cat2]['erros']))
        print ("\nPrecisao:")
        print (cat1+": \t" + str(score['confusion_matrix'][cat1]['precisao']))
        print (cat2+": \t" + str(score['confusion_matrix'][cat2]['precisao']))